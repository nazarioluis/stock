var validateIsUnique = function (cols, msg, pks) {
  return function (next) {
    var self = this
    var conditions = {where: {}}
    cols.forEach(function(col){
      conditions.where[col] = self[col];
    })
    if(!pks){
      conditions.where['id'] = {$ne: self['id']}
    }else{
      pks.forEach(function (pk){
        conditions.where[pk] != {$ne: self[pk]}
      })
    }
    return self.Model.count(conditions).then(function (found) {
        return (found !== 0) ? next(eval(msg)) : next()
    })
  }
}

module.exports = function (Sequelize) {
    Sequelize = !Sequelize ? require('sequelize') : Sequelize
    Sequelize.prototype.validateIsUnique = validateIsUnique
    return Sequelize
}
