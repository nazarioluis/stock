const $ = require('jquery')
const qz = require('qz-tray/qz-tray')
const {KEYUTIL,KJUR,stob64,hextorstr} = require('jsrsasign')
const rsu = require('jsrsasign-util')

function setUpPrinter(){
  qz.security.setCertificatePromise(function(resolve, reject) {
     $.ajax(__dirname+"/qz-ssl/override.crt").then(resolve, reject)
  })

  qz.security.setSignaturePromise(function(toSign) {
       return function(resolve, reject) {
           try {
             var pem = rsu.readFile(__dirname+'/qz-ssl/key.pem')
             var pk = KEYUTIL.getKey(pem)
             var sig = new KJUR.crypto.Signature({"alg": "SHA1withRSA"})
             sig.init(pk)
             sig.updateString(toSign)
             var hex = sig.sign()
             resolve(stob64(hextorstr(hex)))
           } catch (err) {
             console.error(err)
             reject(err)
           }
       }
  })
  qz.api.setPromiseType(require('q').Promise)
  qz.api.setWebSocketType(require('ws'))
}

setUpPrinter()

exports.imprimirHTML = (contenido,copias) => {
  qz.websocket.connect()
  .then(qz.printers.getDefault)
  .then(function(defaultPrinter) {
    console.log(defaultPrinter);
    var config = qz.configs.create(defaultPrinter, { copies: copias })
    var data = [{
       type: 'html',
       format: 'plain', // or 'file'
       data: contenido
    }]
    qz.print(config, data)
    .then(qz.websocket.disconnect)
    .catch(function(e) {
      console.error(e)
      qz.websocket.disconnect()
    })
  })
}
