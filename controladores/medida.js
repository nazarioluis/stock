window.$ = window.jQuery = require('jquery')
const {Medida} = require('../modelo/modelos.js')//Modelos
const util = require('../extra/util')//Utilidades
const auditoria = require('./auditoria')

/**************************/
//Libreria css http://getbootstrap.com/css/
//npm install bootstrap
require('bootstrap')
/**************************/

/**************************/
//Libreria para validar formularios http://1000hz.github.io/bootstrap-validator/
//npm install bootstrap-validator
require('bootstrap-validator')
/**************************/

let medidaAttr//Atributo utilizado para almacenar la instacia del objeto a ser modificado


util.enterNavigation();//Usar enter para navegar el form

//Manejo de evento submit del formulario
$('#f_medida').validator().submit(function( event ) {
  if (!event.isDefaultPrevented()) {
    util.mayus('f_medida')
    var data = $('#f_medida').serializeJSON({checkboxUncheckedValue: "false"})
    if(!medidaAttr){
      insertar(data)
    }else{
      modificar(data)
    }
    event.preventDefault()
  }
})

//Recupera el id del objeto al seleccionar una fila de la tabla
$('#t_medida tbody').on('click', 'tr', function () {
    let id = ($(this).find('td').html())
    recuperarPorId(id)
})

//Restaura vista original
$('#b_cancelar').on('click', restauraValores )

$('#b_eliminar').on('click', function () {
  if(confirm('Esta seguro que desea eliminar la medida:\n['+medidaAttr.id+'] - '+medidaAttr.descripcion+'?')){
      eliminar()
  }
})

//Consultar por cedula
$('#descripcion').keydown(function(e){
    if(e.keyCode == 120 || e.keyCode == 13 || e.keyCode == 9)//f9 o enter o tabulador
    {
      let descripcion = $('#descripcion').val()
      recuperarPorDescripcion(descripcion)
    }
});

//Restaura vista original
$('#buscador').keyup( function() {
  util.delay(function(){
    filtrar()
  }, 500 )
})

//Restarurar valores por defecto
function restauraValores(){
  $('#b_guardar').html('<i class="glyphicon glyphicon-ok-circle"></i> Guardar')
  $('#b_cancelar').hide()
  $('#b_eliminar').hide()
  medidaAttr = null
  $("#f_medida")[0].reset()//limpia el formulario
  $("#acepta_decimal").attr("checked",false)
}

//Inserta los datos en la tabla de la base de datos
function insertar(data){
  Medida.sync().then(() => {
    return Medida.create(data)
  }).then(data => {
    auditoria.insertarAuditoria("medida","Nueva medida #"+data.id)
    restauraValores()
    filtrar()
  }, function(err) {
    alert(err.errors[0].message)
  })
}

//Modificar los datos en la tabla de la base de datos
function modificar(data){
  return medidaAttr.updateAttributes(data)
  .then(function() {
    auditoria.insertarAuditoria("medida","Modificacion de medida #"+medidaAttr.id)
    filtrar()
    restauraValores()
  }, function(err) {
    alert(err.errors[0].message)
  })
}

function eliminar(){
  medidaAttr.destroy().then(function () {
    auditoria.insertarAuditoria("medida","Eliminacion de medida #"+medidaAttr.id)
    restauraValores()
    filtrar()
  })
}


function filtrar(){
  let filtro = $.trim($('#buscador').val())
  let medidas=[]
  if(filtro){
    let condiciones = {
                        descripcion: {$like: '%' + filtro.toUpperCase() + '%'},
                      }
    Medida.findAll({
      attributes:['id','descripcion','unid_medida','acepta_decimal'],
      where: { $or:condiciones }
    }).then(results => {
      results.forEach(function (medida) {
        medida.dataValues.acepta_decimal_str =
         (medida.dataValues.acepta_decimal)?'Si':'No'
        medidas.push(medida.dataValues)
      });
      util.buildTable(medidas, 't_medida')
    })
  }else{
    util.buildTable([], 't_medida')
  }
}

//Se recupera por id y se prepara para edicion o eliminacion
function recuperarPorId(id){
  Medida.findById(id).then(result => {
    prepararParaEdicion(result)
  })
}

function recuperarPorDescripcion(descripcion){
  Medida.findOne({where:{'descripcion':descripcion}}).then(result => {
    if (result) prepararParaEdicion(result)
  })
}

function prepararParaEdicion(result){
  restauraValores()
  medidaAttr = result
  util.cargarFormulario(result.dataValues,"f_medida")
  $('a[href="#formulario"]').tab('show');
  $('#b_guardar').html('<i class="glyphicon glyphicon-ok-circle"></i> Actualizar');
  $('#b_cancelar').show()
  $('#b_eliminar').show()
}
