window.$ = window.jQuery = require('jquery')
const {Deposito} = require('../modelo/modelos.js')//Modelos
const util = require('../extra/util')//Utilidades
const auditoria = require('./auditoria')

/**************************/
//Libreria css http://getbootstrap.com/css/
//npm install bootstrap
require('bootstrap')
/**************************/

/**************************/
//Libreria para validar formularios http://1000hz.github.io/bootstrap-validator/
//npm install bootstrap-validator
require('bootstrap-validator')
/**************************/

let depositoAttr//Atributo utilizado para almacenar la instacia del objeto a ser modificado


util.enterNavigation();//Usar enter para navegar el form

//Manejo de evento submit del formulario
$('#f_deposito').validator().submit(function( event ) {
  if (!event.isDefaultPrevented()) {
    util.mayus('f_deposito')
    var data = $('#f_deposito').serializeJSON({checkboxUncheckedValue: "false"})
    if(!depositoAttr){
      insertar(data)
    }else{
      modificar(data)
    }
    event.preventDefault()
  }
})

//Recupera el id del objeto al seleccionar una fila de la tabla
$('#t_deposito tbody').on('click', 'tr', function () {
    let id = ($(this).find('td').html())
    recuperarPorId(id)
})

//Restaura vista original
$('#b_cancelar').on('click', restauraValores )

$('#b_eliminar').on('click', function () {
  if(confirm('Esta seguro que desea eliminar el deposito:\n['+depositoAttr.id+'] - '+depositoAttr.descripcion+'?')){
      eliminar()
  }
})

//Restaura vista original
$('#buscador').keyup( function() {
  util.delay(function(){
    filtrar()
  }, 500 )
})

//Restarurar valores por defecto
function restauraValores(){
  $('#b_guardar').html('<i class="glyphicon glyphicon-ok-circle"></i> Guardar')
  $('#b_cancelar').hide()
  $('#b_eliminar').hide()
  depositoAttr = null
  $("#f_deposito")[0].reset()//limpia el formulario
  $("#estado").attr("checked",true)
}

//Inserta los datos en la tabla de la base de datos
function insertar(data){
  Deposito.sync().then(() => {
    return Deposito.create(data)
  }).then(data => {
    auditoria.insertarAuditoria("deposito","Nuevo deposito #"+data.id)
    restauraValores()
    filtrar()
  }, function(err) {
    alert(err.errors[0].message)
  })
}

//Modificar los datos en la tabla de la base de datos
function modificar(data){
  return depositoAttr.updateAttributes(data)
  .then(function() {
    auditoria.insertarAuditoria("deposito","Modificacion de deposito #"+depositoAttr.id)
    filtrar()
    restauraValores()
  }, function(err) {
    alert(err.errors[0].message)
  })
}

function eliminar(){
  depositoAttr.destroy().then(function () {
    auditoria.insertarAuditoria("deposito","Eliminacion de deposito #"+depositoAttr.id)
    restauraValores()
    filtrar()
  })
}
function filtrar(){
  let filtro = $.trim($('#buscador').val())
  let depositos=[]
  if(filtro){
    let condiciones = {
                        descripcion: {$like: '%' + filtro.toUpperCase() + '%'},
                        ubicacion: {$like: '%' + filtro.toUpperCase() + '%'},
                      }
    Deposito.findAll({
      attributes:['id','descripcion','ubicacion','estado'],
      where: { $or:condiciones }
    }).then(results => {
      results.forEach(function (deposito) {
        deposito.dataValues.estado_str = (deposito.dataValues.estado)?'Activo':'Inactivo'
        depositos.push(deposito.dataValues)
      });
      util.buildTable(depositos, 't_deposito')
    })
  }else{
    util.buildTable([], 't_deposito')
  }
}

//Se recupera por id y se prepara para edicion o eliminacion
function recuperarPorId(id){
  Deposito.findById(id).then(result => {
    prepararParaEdicion(result)
  })
}

function recuperarPorDescripcion(descripcion){
  Deposito.findOne({where:{'descripcion':descripcion}}).then(result => {
    if (result) prepararParaEdicion(result)
  })
}

function prepararParaEdicion(result){
  restauraValores()
  depositoAttr = result
  util.cargarFormulario(result.dataValues,"f_deposito")
  $('a[href="#formulario"]').tab('show');
  $('#b_guardar').html('<i class="glyphicon glyphicon-ok-circle"></i> Actualizar');
  $('#b_cancelar').show()
  $('#b_eliminar').show()
}
