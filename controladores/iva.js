window.$ = window.jQuery = require('jquery')
const {Iva} = require('../modelo/modelos.js')//Modelos
const util = require('../extra/util')//Utilidades
const auditoria = require('./auditoria')

/**************************/
//Libreria css http://getbootstrap.com/css/
//npm install bootstrap
require('bootstrap')
/**************************/

/**************************/
//Libreria para validar formularios http://1000hz.github.io/bootstrap-validator/
//npm install bootstrap-validator
require('bootstrap-validator')
/**************************/

let ivaAttr//Atributo utilizado para almacenar la instacia del objeto a ser modificado

util.enterNavigation();//Usar enter para navegar el form

//Manejo de evento submit del formulario
$('#f_iva').validator().submit(function( event ) {
  if (!event.isDefaultPrevented()) {
    util.mayus('f_iva')
    var data = $('#f_iva').serializeJSON({checkboxUncheckedValue: "false"})
    if(!ivaAttr){
      insertar(data)
    }else{
      modificar(data)
    }
    event.preventDefault()
  }
})

//Recupera el id del objeto al seleccionar una fila de la tabla
$('#t_iva tbody').on('click', 'tr', function () {
    let id = ($(this).find('td').html())
    recuperarPorId(id)
})

//Restaura vista original
$('#b_cancelar').on('click', restauraValores )

$('#b_eliminar').on('click', function () {
  if (confirm('Esta seguro que desa eliminar el registro?')) {
      eliminar()

  }
})

//Restaura vista original
$('#buscador').keyup( function() {
  util.delay(function(){
    filtrar()
  }, 500 )
})

//Restarurar valores por defecto
function restauraValores(){
  $('#b_guardar').html('<i class="glyphicon glyphicon-ok-circle"></i> Guardar')
  $('#b_cancelar').hide()
  $('#b_eliminar').hide()
  ivaAttr = null
  $("#f_iva")[0].reset()//limpia el formulario
}

//Inserta los datos en la tabla de la base de datos
function insertar(data){
  Iva.sync().then(() => {
    return Iva.create(data)
  }).then(data => {
    auditoria.insertarAuditoria("iva","Nuevo iva #"+data.id)
    restauraValores()
    filtrar()
  }, function(err) {
    alert(err.errors[0].message)
  })
}

//Modificar los datos en la tabla de la base de datos
function modificar(data){
  return ivaAttr.updateAttributes(data)
  .then(function() {
    auditoria.insertarAuditoria("iva","Modificacion de iva #"+ivaAttr.id)
    filtrar()
    restauraValores()
  }, function(err) {
    alert(err.errors[0].message)
  })
}

function eliminar(){
  ivaAttr.destroy().then(function () {
    auditoria.insertarAuditoria("iva","Eliminacion de iva #"+ivaAttr.id)
    restauraValores()
    filtrar()
  })
}

function filtrar(){
  let filtro = $.trim($('#buscador').val())
  let ivas=[]
  if(filtro){
    let condiciones = {
                        descripcion: {$like: '%' + filtro.toUpperCase() + '%'},
                      }
    Iva.findAll({
      attributes:['id','descripcion','porcentaje'],
      where: { $or:condiciones }
    }).then(results => {
      results.forEach(function (iva) {
        ivas.push(iva.dataValues)
      });
      util.buildTable(ivas, 't_iva')
    })
  }else{
    util.buildTable([], 't_iva')
  }
}

//Se recupera por id y se prepara para edicion o eliminacion
function recuperarPorId(id){
  Iva.findById(id).then(result => {
    prepararParaEdicion(result)
  })
}

function prepararParaEdicion(result){
  restauraValores()
  ivaAttr = result
  util.cargarFormulario(result.dataValues,"f_iva")
  $('a[href="#formulario"]').tab('show');
  $('#b_guardar').html('<i class="glyphicon glyphicon-ok-circle"></i> Actualizar');
  $('#b_cancelar').show()
  $('#b_eliminar').show()
}
