window.$ = window.jQuery = require('jquery')
const {Proveedor,Ciudad} = require('../modelo/modelos.js')//Modelos
const util = require('../extra/util')//Utilidades
const auditoria = require('./auditoria')

/**************************/
//Libreria css http://getbootstrap.com/css/
//npm install bootstrap
require('bootstrap')
/**************************/

/**************************/
//Libreria para validar formularios http://1000hz.github.io/bootstrap-validator/
//npm install bootstrap-validator
require('bootstrap-validator')
/**************************/

let proveedorAttr//Atributo utilizado para almacenar la instacia del objeto a ser modificado
let ciudad

util.enterNavigation();//Usar enter para navegar el form

cargarCiudad()

//Manejo de evento submit del formulario
$('#f_proveedor').validator().submit(function( event ) {
  if (!event.isDefaultPrevented()) {
    util.mayus('f_proveedor')
    var data = $('#f_proveedor').serializeJSON({checkboxUncheckedValue: "false"})
    if(!proveedorAttr){
      insertar(data)
    }else{
      modificar(data)
    }
    event.preventDefault()
  }
})

//Recupera el id del objeto al seleccionar una fila de la tabla
$('#t_proveedor tbody').on('click', 'tr', function () {
    let id = ($(this).find('td').html())
    recuperarPorId(id)
})

//Restaura vista original
$('#b_cancelar').on('click', restauraValores )

$('#b_eliminar').on('click', function () {
  if (confirm('Esta seguro que desa eliminar el registro?')) {
      eliminar()
  }
})

//Consultar por cedula
$('#documento').keydown(function(e){
    if(e.keyCode == 120 || e.keyCode == 13 || e.keyCode == 9)//f9 o enter o tabulador
    {
      let documento = $('#documento').val()
      recuperarPorDocumento(documento)
    }
});

//Restaura vista original
$('#buscador').keyup( function() {
  util.delay(function(){
    filtrar()
  }, 500 )
})

//Restarurar valores por defecto
function restauraValores(){
  $('#b_guardar').html('<i class="glyphicon glyphicon-ok-circle"></i> Guardar')
  $('#b_cancelar').hide()
  $('#b_eliminar').hide()
  proveedorAttr = null
  $("#f_proveedor")[0].reset()//limpia el formulario
}

//Inserta los datos en la tabla de la base de datos
function insertar(data){
    if (data.ciudadId === "") data.ciudadId= null
    Proveedor.sync().then(() => {
    return Proveedor.create(data)
  }).then(data => {
    auditoria.insertarAuditoria("proveedor","Nuevo proveedor #"+data.id)
    restauraValores()
    filtrar()
  }, function(err) {
    alert(err.errors[0].message)
  })
}

//Modificar los datos en la tabla de la base de datos
function modificar(data){
  if (data.ciudadId === "") data.ciudadId= null
  return proveedorAttr.updateAttributes(data)
  .then(function() {
    auditoria.insertarAuditoria("proveedor","Modificacion de proveedor #"+proveedorAttr.id)
    filtrar()
    restauraValores()
  }, function(err) {
    alert(err.errors[0].message)
  })
}

function eliminar(){
  proveedorAttr.destroy().then(function () {
    auditoria.insertarAuditoria("proveedor","Eliminacion de proveedor #"+proveedorAttr.id)
    restauraValores()
    filtrar()
  })
}

function cargarCiudad(){
  ciudades=[{"id":"","descripcion":"SELECCIONAR CIUDAD"}]
  Ciudad.findAll().then(results => {
    results.forEach(function (ciudad) {
      ciudades.push(ciudad.dataValues)
    });
    util.buildSelect(ciudades, 's_ciudad')
  })
}

function filtrar(){
  let filtro = $.trim($('#buscador').val())
  let proveedores=[]
  if(filtro){
    let condiciones = {
                        razon_social: {$like: '%' + filtro.toUpperCase() + '%'},
                        documento: {$like: '%' + filtro + '%'}
                      }
    Proveedor.findAll({
      attributes:['id','razon_social','documento','direccion','email','telefono1','telefono2'],
      where: { $or:condiciones }
    }).then(results => {
      results.forEach(function (proveedor) {
        proveedores.push(proveedor.dataValues)
      });
      util.buildTable(proveedores, 't_proveedor')
    })
  }else{
    util.buildTable([], 't_proveedor')
  }
}

//Se recupera por id y se prepara para edicion o eliminacion
function recuperarPorId(id){
  Proveedor.findById(id).then(result => {
    prepararParaEdicion(result)
  })
}

function recuperarPorDocumento(documento){
  Proveedor.findOne({where:{'documento':documento}}).then(result => {
    if (result) prepararParaEdicion(result)
  })
}

function prepararParaEdicion(result){
  restauraValores()
  proveedorAttr = result
  util.cargarFormulario(result.dataValues,"f_proveedor")
  $('a[href="#formulario"]').tab('show');
  $('#b_guardar').html('<i class="glyphicon glyphicon-ok-circle"></i> Actualizar');
  $('#b_cancelar').show()
  $('#b_eliminar').show()
}
