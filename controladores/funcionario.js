window.$ = window.jQuery = require('jquery')
const {Funcionario} = require('../modelo/modelos.js')//Modelos
const util = require('../extra/util')//Utilidades
const auditoria = require('./auditoria')

/**************************/
//Libreria css http://getbootstrap.com/css/
//npm install bootstrap
require('bootstrap')
/**************************/

/**************************/
//Libreria para validar formularios http://1000hz.github.io/bootstrap-validator/
//npm install bootstrap-validator
require('bootstrap-validator')
/**************************/

let funcionarioAttr//Atributo utilizado para almacenar la instacia del objeto a ser modificado


util.enterNavigation();//Usar enter para navegar el form

//Manejo de evento submit del formulario
$('#f_funcionario').validator().submit(function( event ) {
  if (!event.isDefaultPrevented()) {
    util.mayus('f_funcionario')
    var data = $('#f_funcionario').serializeJSON({checkboxUncheckedValue: "false"})
    if(!funcionarioAttr){
      insertar(data)
    }else{
      modificar(data)
    }
    event.preventDefault()
  }
})

//Recupera el id del objeto al seleccionar una fila de la tabla
$('#t_funcionario tbody').on('click', 'tr', function () {
    let id = ($(this).find('td').html())
    recuperarPorId(id)
})

//Restaura vista original
$('#b_cancelar').on('click', restauraValores )

$('#b_eliminar').on('click', function () {
  if(confirm('Esta seguro que desea eliminar al funcionario:\n['+funcionarioAttr.id+'] - '+funcionarioAttr.nombre+'?')){
      eliminar()
  }
})

//Consultar por cedula
$('#documento').keydown(function(e){
    if(e.keyCode == 120 || e.keyCode == 13 || e.keyCode == 9)//f9 o enter o tabulador
    {
      let documento = $('#documento').val()
      recuperarPorDocumento(documento)
    }
});

//Restaura vista original
$('#buscador').keyup( function() {
  util.delay(function(){
    filtrar()
  }, 500 )
})

//Restarurar valores por defecto
function restauraValores(){
  $('#b_guardar').html('<i class="glyphicon glyphicon-ok-circle"></i> Guardar')
  $('#b_cancelar').hide()
  $('#b_eliminar').hide()
  funcionarioAttr = null
  $("#f_funcionario")[0].reset()//limpia el formulario
  $("#estado").attr("checked",true)
}

//Inserta los datos en la tabla de la base de datos
function insertar(data){
  Funcionario.sync().then(() => {
    return Funcionario.create(data)
  }).then(data => {
    auditoria.insertarAuditoria("funcionario","Nuevo funcionario #"+data.id)
    restauraValores()
    filtrar()
  }, function(err) {
    alert(err.errors[0].message)
  })
}

//Modificar los datos en la tabla de la base de datos
function modificar(data){
  return funcionarioAttr.updateAttributes(data)
  .then(function() {
    auditoria.insertarAuditoria("funcionario","Modificacion de funcionario #"+funcionarioAttr.id)
    filtrar()
    restauraValores()
  }, function(err) {
    alert(err.errors[0].message)
  })
}

function eliminar(){
  funcionarioAttr.destroy().then(function () {
    auditoria.insertarAuditoria("funcionario","Eliminacion de funcionario #"+funcionarioAttr.id)
    restauraValores()
    filtrar()
  })
}

function filtrar(){
  let filtro = $.trim($('#buscador').val())
  let funcionarios=[]
  if(filtro){
    let condiciones = {
                        nombre: {$like: '%' + filtro.toUpperCase() + '%'},
                        documento: {$like: '%' + filtro + '%'}
                      }
    Funcionario.findAll({
      attributes:['id','nombre','documento','email','telefono', 'estado'],
      where: { $or:condiciones }
    }).then(results => {
      results.forEach(function (funcionario) {
        funcionario.dataValues.estado_str = (funcionario.dataValues.estado)?'Activo':'Inactivo'
        funcionarios.push(funcionario.dataValues)
      });
      util.buildTable(funcionarios, 't_funcionario')
    })
  }else{
    util.buildTable([], 't_funcionario')
  }
}

//Se recupera por id y se prepara para edicion o eliminacion
function recuperarPorId(id){
  Funcionario.findById(id).then(result => {
    prepararParaEdicion(result)
  })
}

function recuperarPorDocumento(documento){
  Funcionario.findOne({where:{'documento':documento}}).then(result => {
    if (result) prepararParaEdicion(result)
  })
}

function prepararParaEdicion(result){
  restauraValores()
  funcionarioAttr = result
  util.cargarFormulario(result.dataValues,"f_funcionario")
  $('a[href="#formulario"]').tab('show');
  $('#b_guardar').html('<i class="glyphicon glyphicon-ok-circle"></i> Actualizar');
  $('#b_cancelar').show()
  $('#b_eliminar').show()
}
