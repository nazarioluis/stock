window.$ = window.jQuery = require('jquery')
const {Compra,CompraItem,Producto,Comprobante,Iva,Proveedor} = require('../modelo/modelos.js')//Modelos
const util = require('../extra/util')//Utilidades
const auditoria = require('./auditoria')

/**************************/
//Libreria css http://getbootstrap.com/css/
//npm install bootstrap
require('bootstrap')
/**************************/

/**************************/
//Libreria para validar formularios http://1000hz.github.io/bootstrap-validator/
//npm install bootstrap-validator
require('bootstrap-validator')
/**************************/

let compraAttr; //Atributo utilizado para almacenar la instacia del objeto a ser modificado
let comprobantes;
let compra={};
let ivas;
let productos;
let indiceEdicion;
let items = [];

util.enterNavigation();//Usar enter para navegar el form

cargarComprobante();
cargarProducto();
cargarIva();

let fechaActual = new Date() //fecha el sistema
$('#fechafactura').val(fechaActual.toISOString().split('T')[0])

//Restaura vista original
$('#buscador').keyup( function() {
  util.delay(function(){
    filtrar()
  }, 500 )
})


/**
 * Evento keydown para el campo RUC Cuando se presiona f9 enter o tab, recupera
 * el valor introducido y los pasa a la funcion cargarProveedorPorDocumento
 */
$('#documento').keydown(function(e){
    if(e.keyCode == 120 || e.keyCode == 13 || e.keyCode == 9)//f9 o enter o tabulador
    {
      let documento = $('#documento').val().trim()
      cargarProveedorPorDocumento(documento)
    }
});


/**
 * cargarProveedorPorDocumento - Bunca un proveedor por su documento. Recupera
 * el proveedor lo guarda en la variable proveedorAttr y carga en el formulario
 * @param {String} documento Documento del proveedor
 */
function cargarProveedorPorDocumento(documento){
  Proveedor.findOne({where:{'documento':documento}}).then(result => {
    if (result) {
      result.dataValues.proveedorId = result.id;
      util.cargarFormulario(result.dataValues,"f_compra")
    }
  })
}


/**
 * Evento click sobre el botón guardar Al hacer
 * click llama a la funcion submit del formulario
 */
$('#b_guardar').on('click', function () {
   $('#f_compra').submit()
})

//Manejo de evento submit del formulario
$('#f_compra, #f_totales').validator().submit(function( event ) {
  if (!event.isDefaultPrevented()) {
    util.mayus('f_compra')
    let encabezado = $('#f_compra').serializeJSON({checkboxUncheckedValue: "false"})
    $.extend(compra, encabezado);
    compra.items = JSON.parse(JSON.stringify(items));
    let dif = compra.total_comp - compra.total;
    if (dif === 0) {
      if(!compraAttr){
        insertar(compra);
      } else {
        modificar(compra);
      }
    } else {
      alert("Existe diferencia entre el valor de compra\ningresado y el valor total calculado")
    }
    event.preventDefault()
  }
})


/**
 * insertar - Inserta una compra. Verifica si existe el
 * proveedor, si existe guarda directamente la compra,
 * sino primero crea el proveedor luego se crea la compra
 * @param {Object} data Objeto compra
 */
function insertar(data){
  data.proveedorId=(data.proveedorId==="")?null:data.proveedorId;
  data.numtimbrado=(data.numtimbrado===""||data.numtimbrado==="0")?null:data.numtimbrado;
  data.venctimbrado=(data.venctimbrado==="")?null:data.venctimbrado;
  // se comprueba que exista el proveedor
  if (data.proveedorId) { // si existe
    Compra.create(data,{  // inserta compra con los items incluidos
      include: [{
        model: CompraItem,
        as: 'items'
      }]
    }).then(data => { // cuando se completa la insercion se inserta la auditoria
      auditoria.insertarAuditoria("compra","Nueva compra #"+data.id)
      filtrar()
    }, function(err) {  // en caso de error en el insert de la compra
      alert(err.errors[0].message)
    })
  } else {  //si no existe
    // objeto proveedor que se utiliza para insertar un nuevo proveedor
    // los datos son tomados de la compra
    let proveedor = {
      razon_social: data.razon_social,
      documento: data.documento,
      telefono1: data.telefono1,
      direccion: data.direccion
    }
    return Proveedor.create(proveedor)  // se inserta el proveedor
    // si se completa la insercion, se inserta la
    // auditoria de proveedor y tambien la compra
    .then(p => {  // p tiene los datos del proveedor insertado
      auditoria.insertarAuditoria("proveedor","Nuevo proveedor #"+p.id)
      data.proveedorId = p.id;  // se asigna el valor del p.id a la compra.proveedorId
      return Compra.create(data,{ // se crea la compra con los items incluidos
        include: [{
          model: CompraItem,
          as: 'items'
        }]
      }).then( c => {
          auditoria.insertarAuditoria("compra","Nueva compra #"+c.id)
          // restauraValores()    se debe restauraValores solo con el botón nuevo
          filtrar()
        }, function(err){ // si falla la insercion de la compra
          alert(err.errors[0].message)
        }
      );
    }, function (err) { // si falla la insercion de proveedor
      alert(err.errors[0].message)
    })
  }
}

//Modificar los datos en la tabla de la base de datos
function modificar(data){
  return compraAttr.updateAttributes(data)
  .then(function() {
    auditoria.insertarAuditoria("compra","Modificacion de compra #"+compraAttr.id)
    filtrar()
    restauraValores()
  }, function(err) {
    alert(err.errors[0].message)
  })
}


/**
 * Evento click del botón eliminar. Se pide confirmación
 * del usario luego se llama a la funcion eliminar
 */
$('#b_eliminar').on('click', function () {
  if (confirm('Esta seguro que desa eliminar el registro?')) {
      eliminar()
  }
})


/**
 * eliminar - Elimina una compra
 */
function eliminar(){
  compraAttr.destroy().then(function () {
    auditoria.insertarAuditoria("compra","Eliminacion de compra #"+compraAttr.id)
    restauraValores()
    filtrar()
  })
}

/**Recupera los datos cargados en los campos para crear un objeto
 * y el objeto es pasado a la funcion cargarItem luego se vacian los campos */
$('#f_item').validator().submit(function (event) {
   if (!event.isDefaultPrevented()) {
      let item = $('#f_item').serializeJSON();

      item.descripcion= $("#s_producto option[value='"+item.productoId+"']").text();
      console.log(ivas);
      ivas.forEach(elem => {
        if (elem.id==item.ivaId){
          item.exenta = (elem.porcentaje==="0") ? item.precunit * item.cantidad : 0;
          item.iva_cinco = (elem.porcentaje==="5") ? item.precunit * item.cantidad : 0;
          item.iva_diez = (elem.porcentaje==="10") ? item.precunit * item.cantidad : 0;
        }
      });
      util.mayus('f_item');
      if (indiceEdicion == null) {
         cargaItem(item);
      } else {
         cargaItem(item, indiceEdicion);
      }
      restauraValoresItem();
      event.preventDefault();
   }
})

/**Agrega un item al array de items y carga la tabla con ese item*/
function cargaItem(item, indice) {
   if (indice == null) {
      items.push(item)
   } else {
      $.extend(items[indice], item);
   }
   util.buildTable(items, 't_items')
   calcularTotales()
}


/**
 * calcularTotales - Calcula los totales. Recorre el
 * array de items para realizar calculos de subtotales
 * los ivas y los totales generales y hace los decuantos
 */
function calcularTotales(){
  compra.subtot_ex = 0
  compra.subtot_5 = 0
  compra.subtot_10 = 0
  for (var i = 0; i < items.length; i++) {//Clacula los subtotales
    compra.subtot_ex = compra.subtot_ex + items[i].exenta
    compra.subtot_5 = compra.subtot_5 + items[i].iva_cinco
    compra.subtot_10 = compra.subtot_10 + items[i].iva_diez
  }
  //realiza descuentos si los hay
  realizarDescuento()
  //Calcula iva por subtotal
  compra.iva_5 = Math.round(compra.subtot_5 / 21)
  compra.iva_10 = Math.round(compra.subtot_10 / 11)
  //Calcula los tatales
  compra.total = compra.subtot_ex + compra.subtot_5 + compra.subtot_10
  compra.total_iva = compra.iva_5 + compra.iva_10
  //Carga los datos en los totales
  util.cargarFormulario(compra,"f_totales")
}


/**
 * realizarDescuento - Recupera el formulario para descuentos y
 * serializa los valores para aplicar los descunetos a los subtotales
 */
function realizarDescuento(){
  let descuentos = $('#f_totales').serializeJSON();
  compra.descuento_ex = descuentos.descuento_ex
  compra.descuento5 = descuentos.descuento5
  compra.descuento10 = descuentos.descuento10
  compra.subtot_ex = compra.subtot_ex - descuentos.descuento_ex
  compra.subtot_5 = compra.subtot_5 - descuentos.descuento5
  compra.subtot_10 = compra.subtot_10 - descuentos.descuento10
}

//Evento para editar un item de la tabla de items
$('#t_items tbody').single_double_click('tr', function () {
   /*Evento onClick sobre las filas de la tabla.
   editar un item del array de items*/
   indiceEdicion = $(this).index()
   util.cargarFormulario(items[indiceEdicion], 'f_item')
   $('#b_add').html('<i class="glyphicon glyphicon-ok"></i> OK');
   $('#b_cancelar_item').show()
}, function () {
   /*Evento DoubleClick sobre las filas de la tabla.
   Borra la fila de la tabla y el item del array de items*/
   if (indiceEdicion != null) {
      alert("No se puede eliminar items mientras se modifica uno de ellos")
      return
   }
   let indice = $(this).index()
   items.splice(indice, 1)
   $(this).remove()
   calcularTotales()
})

//Restaura vista original
$('#b_cancelar').on('click', restauraValores )

//Restarurar valores por defecto
function restauraValores(){
  $('#b_guardar').html('<i class="glyphicon glyphicon-ok-circle"></i> Guardar')
  $('#b_cancelar').hide()
  $('#b_eliminar').hide()
  compraAttr = null
  $("#f_compra")[0].reset()//limpia el formulario
  // se pasa la fecha actual a formato yyyy/mm/dd para que muestre en el input type date
  $('#fechafactura').val(fechaActual.toISOString().split('T')[0])
}

// Restaura formulario item
$('#b_cancelar_item').on('click', restauraValoresItem)

//Restarurar valores por defecto
function restauraValoresItem() {
  indiceEdicion = null
  $('#f_item')[0].reset()
  $("#item_ref").val("0");
  $('#b_add').html('<i class="glyphicon glyphicon-plus"></i>')
  $('#b_cancelar_item').hide()
}

function cargarComprobante(){
  comprobantes=[{"id":"","descripcion":"SELECCIONAR COMPROBANTE"}]
  Comprobante.findAll().then(results => {
    results.forEach(function (comprobante) {
      comprobantes.push(comprobante.dataValues)
    });
    util.buildSelect(comprobantes, 's_comprobante')
  })
}

function cargarProducto(){
  productos=[{"id":"","descripcion":"SELECCIONAR PRODUCTO"}]
  Producto.findAll().then(results => {
    results.forEach(function (producto) {
      productos.push(producto.dataValues)
    });
    util.buildSelect(productos, 's_producto')
  })
}

function cargarIva(){
  ivas=[{"id":"","descripcion":"SELECCIONAR IMPUESTO"}]
  Iva.findAll().then(results => {
    results.forEach(function (iva) {
      ivas.push(iva.dataValues)
    });
    util.buildSelect(ivas, 's_iva')
  })
}

//filtra y muestra los datos
function filtrar(){
  let filtro = $.trim($('#buscador').val())
  let compras=[]
  if(filtro){
    let condiciones = {
                        razon_social: {$like: '%' + filtro.toUpperCase() + '%'},
                        ruc: {$like: '%' + filtro + '%'},
                        numcomprobante: {$like: '%' + filtro + '%'}
                      }
    Compra.findAll({
      where: { $or:condiciones },
      include: [{model:Comprobante,attributes:['descripcion']}]
    }).then(results => {
      results.forEach(function (compra) {
        compras.push({
          'id' : compra.id,
          'ruc' : compra.ruc,
          'razon_social' : compra.razon_social,
          'fechafactura' : compra.fechafactura.toLocaleDateString('en-GB'),
          'numcomprobante' : compra.numcomprobante,
          'numtimbrado' : compra.numtimbrado,
          'venctimbrado' : compra.venctimbrado.toLocaleDateString('en-GB'),
          'comprobante' : compra.comprobante.descripcion,
          'exenta' : compra.subtot_ex,
          'subtot_5' : compra.subtot_5,
          'subtot_10' : compra.subtot_10,
          'total' : compra.total,
          'iva5' : compra.iva_5,
          'iva10' : compra.iva_10,
          'ivatotal' : compra.total_iva
        })
      });
      util.buildTable(compras, 't_compra')
    })
  }else{
    util.buildTable([], 't_compra')
  }
}

//Recupera el id del objeto al seleccionar una fila de la tabla
$('#t_compra tbody').on('click', 'tr', function () {
    let id = ($(this).find('td').html())
    recuperarPorId(id)
})

//Se recupera por id y se prepara para edicion o eliminacion
function recuperarPorId(id){
  Compra.findById(id).then(result => {
    prepararParaEdicion(result)
  })
}

function recuperarPorDocumento(documento){
  Compra.findOne(
  {
    attributes:['id','razon_social','documento','direccion','email'],
    where:{'documento':documento}}).then(result => {
    if (result) prepararParaEdicion(result)
  })
}

function recuperarPorNumeroComprobante(numcomprobante){
  Compra.findOne(
    {
      where:{'numcomprobante':numcomprobante}}).then(result => {
    if (result) prepararParaEdicion(result)
  })
}

function prepararParaEdicion(result){
  restauraValores()
  compraAttr = result
  util.cargarFormulario(result.dataValues,"f_compra")
  // se pasa la fecha a formato yyyy/mm/dd para que muestre en el input type date despues del util.cargar formulario
  $('#fechafactura').val(result.dataValues.fechafactura.toISOString().split('T')[0])
  $('#venctimbrado').val(result.dataValues.venctimbrado.toISOString().split('T')[0])
  $('a[href="#formulario"]').tab('show');
  $('#b_guardar').html('<i class="glyphicon glyphicon-ok-circle"></i> Actualizar');
  $('#b_cancelar').show()
  $('#b_eliminar').show()
}
