window.$ = window.jQuery = require('jquery')
const {Ciudad,Departamento} = require('../modelo/modelos.js')//Modelos
const util = require('../extra/util')//Utilidades
const auditoria = require('./auditoria')//Auditoria

/**************************/
//Libreria css http://getbootstrap.com/css/
//npm install bootstrap
require('bootstrap')
/**************************/

/**************************/
//Libreria para validar formularios http://1000hz.github.io/bootstrap-validator/
//npm install bootstrap-validator
require('bootstrap-validator')
/**************************/

let ciudadAttr//Atributo utilizado para almacenar la instacia del objeto a ser modificado
let departamentos

util.enterNavigation();//Usar enter para navegar el form

cargarDepartamentos()

//Manejo de evento submit del formulario
$('#f_ciudad').validator().submit(function( event ) {
  if (!event.isDefaultPrevented()) {
    util.mayus('f_ciudad')
    var data = $('#f_ciudad').serializeJSON({checkboxUncheckedValue: "false"})
    if(!ciudadAttr){
      insertar(data)
    }else{
      modificar(data)
    }
    event.preventDefault()
  }
})

//Recupera el id del objeto al seleccionar una fila de la tabla
$('#t_ciudad tbody').on('click', 'tr', function () {
    let id = ($(this).find('td').html())
    recuperarPorId(id)
})

//Restaura vista original
$('#b_cancelar').on('click', restauraValores )

$('#b_eliminar').on('click', function () {
  if (confirm('Esta seguro que desea eliminar el registro?')) {
      eliminar()
  }
})

//Restaura vista original
$('#buscador').keyup( function() {
  util.delay(function(){
    filtrar()
  }, 500 )
})

//Restarurar valores por defecto
function restauraValores(){
  $('#b_guardar').html('<i class="glyphicon glyphicon-ok-circle"></i> Guardar')
  $('#b_cancelar').hide()
  $('#b_eliminar').hide()
  ciudadAttr = null
  $("#f_ciudad")[0].reset()//limpia el formulario
}

//Inserta los datos en la tabla de la base de datos
function insertar(data){
  if (data.departamentoId === "") data.departamentoId= null
  Ciudad.sync().then(() => {
    return Ciudad.create(data)
  }).then(data => {
    auditoria.insertarAuditoria("ciudad","Nueva ciudad #"+data.id)
    restauraValores()
    filtrar()
  }, function(err) {
    alert(err.errors[0].message)
  })
}

//Modificar los datos en la tabla de la base de datos
function modificar(data){
  if (data.departamentoId === "") data.departamentoId= null
  return ciudadAttr.updateAttributes(data)
  .then(function() {
    auditoria.insertarAuditoria("ciudad","Modificacion de ciudad #"+ciudadAttr.id)
    filtrar()
    restauraValores()
  }, function(err) {
    alert(err.errors[0].message)
  })
}

function eliminar(){
  ciudadAttr.destroy().then(function(){
  auditoria.insertarAuditoria("ciudad","Eliminacion de ciudad #"+ciudadAttr.id)
  restauraValores()
  filtrar()
  })
}

function cargarDepartamentos(){
  departamentos=[{"id":"","descripcion":"SELECCIONAR DEPARTAMENTO"}]
  Departamento.findAll().then(results => {
    results.forEach(function (departamento) {
      departamentos.push(departamento.dataValues)
    });
    util.buildSelect(departamentos, 's_departamento')
  })
}

function filtrar(){
  let filtro = $.trim($('#buscador').val())
  let ciudades=[]
  if(filtro){
    let condiciones = {
                        descripcion: {$like: '%' + filtro.toUpperCase() + '%'},
                      }
    Ciudad.findAll({
      attributes:['id','descripcion'],
      where: { $or:condiciones },
      include: [{model:Departamento,attributes:['descripcion']}]
    }).then(results => {
      results.forEach(function (ciudad) {
        ciudades.push({
          'id' : ciudad.id,
          'descripcion' : ciudad.descripcion,
          'departamento' : (ciudad.departamento)? ciudad.departamento.descripcion:""
        })
      });
      util.buildTable(ciudades, 't_ciudad')
    })
  }else{
    util.buildTable([], 't_ciudad')
  }
}

//Se recupera por id y se prepara para edicion o eliminacion
function recuperarPorId(id){
  Ciudad.findById(id).then(result => {
    prepararParaEdicion(result)
  })
}

function prepararParaEdicion(result){
  restauraValores()
  ciudadAttr = result
  util.cargarFormulario(result.dataValues,"f_ciudad")
  $('a[href="#formulario"]').tab('show');
  $('#b_guardar').html('<i class="glyphicon glyphicon-ok-circle"></i> Actualizar');
  $('#b_cancelar').show()
  $('#b_eliminar').show()
}
