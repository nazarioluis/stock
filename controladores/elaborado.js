window.$ = window.jQuery = require('jquery')
const {Elaborado, ElaboradoItem,Mat_prima,Medida} = require('../modelo/modelos.js')//Modelos
const util = require('../extra/util')//Utilidades
const auditoria = require('./auditoria')

/**************************/
//Libreria css http://getbootstrap.com/css/
//npm install bootstrap
require('bootstrap')
/**************************/

/**************************/
//Libreria para validar formularios http://1000hz.github.io/bootstrap-validator/
//npm install bootstrap-validator
require('bootstrap-validator')
/**************************/

let elaboradoAttr//Atributo utilizado para almacenar la instacia del objeto a ser modificado
let mat_primas

/** Atribuos utilizados para el formulario de items */
let indiceEdicion = null
let deleted_items_ids = []
let items = []

util.enterNavigation();//Usar enter para navegar el form

cargarMat_prima()

/**Recupera los datos cargados en los campos para crear un objeto
 * y el objeto es pasado a la funcion cargarItem
 * luego se vacian los campos */
$('#f_item').validator().submit(function (event) {
   if (!event.isDefaultPrevented()) {
      let item = $('#f_item').serializeJSON()
      item.descripcion= $("#s_mat_prima option[value='"+item.materiaId+"']").text()
      item.costo= item.cantidad*mat_primas[$("#s_mat_prima").prop("selectedIndex")].precomprapro
      item.unid_medida= mat_primas[$("#s_mat_prima").prop("selectedIndex")].medida.unid_medida
      util.mayus('f_item')
      if (indiceEdicion == null) {
         cargaItem(item)
      } else {
         cargaItem(item, indiceEdicion)
      }
      restauraValoresItem()
      event.preventDefault()
   }
})

/**Agrega un item al array de items y carga la tabla con ese item*/
function cargaItem(item, indice) {
   if (indice == null) {
      items.push(item)
   } else {
      items[indice] = item
   }
   util.buildTable(items, 't_items')
}

//Evento para editar un item de la tabla de items
$('#t_items tbody').single_double_click('tr', function () {
   /*Evento onClick sobre las filas de la tabla.
   editar un item del array de items*/
   indiceEdicion = $(this).index()
   util.cargarFormulario(items[indiceEdicion], 'f_item')
   $('#b_add').html('<i class="glyphicon glyphicon-ok"></i> OK');
   $('#b_cancelar_item').show()
}, function () {
   /*Evento DoubleClick sobre las filas de la tabla.
   Borra la fila de la tabla y el item del array de items*/
   if (indiceEdicion != null) {
      alert("No se puede eliminar items mientras se modifica uno de ellos")
      return
   }
   let indice = $(this).index()
   if (parseInt(items[indice].id)) {
      deleted_items_ids.push(items[indice].id)
   }
   items.splice(indice, 1)
   $(this).remove()
})



/** Evento onClick para guardar todos los datos */
$('#b_guardar').on('click', function () {
   $('#f_elaborado').submit()
})

//Manejo de evento submit del formulario
$('#f_elaborado').validator().submit(function( event ) {
  if (!event.isDefaultPrevented()) {
    util.mayus('f_elaborado')
    var data = $('#f_elaborado').serializeJSON({checkboxUncheckedValue: "false"})
    if(!elaboradoAttr){
      data.items = items
      insertar(data)
    }else{
      modificar(data)
    }
    event.preventDefault()
  }
})

//Recupera el id del objeto al seleccionar una fila de la tabla
$('#t_elaborado tbody').on('click', 'tr', function () {
    let id = ($(this).find('td').html())
    recuperarPorId(id)
})

//Restaura vista original
$('#b_cancelar').on('click', restauraValores )

$('#b_eliminar').on('click', function () {
  if (confirm('Esta seguro que desa eliminar el registro?')) {
      eliminar()
  }
})

//Restaura vista original
$('#buscador').keyup( function() {
  util.delay(function(){
    filtrar()
  }, 500 )
})

//Restarurar valores por defecto
function restauraValores(){
  $('#b_guardar').html('<i class="glyphicon glyphicon-ok-circle"></i> Guardar')
  $('#b_cancelar').hide()
  elaboradoAttr = null
  items.length = 0
  $("input[type=hidden]").val("");
  $("#f_elaborado")[0].reset() //limpia el formulario
  $('#f_item')[0].reset()
  $("#item_id").val("");
  util.buildTable(items, 't_items')
  restauraValoresItem()
}

//Restarurar valores por defecto
function restauraValoresItem() {
   indiceEdicion = null
   deleted_items_ids = []
   $('#f_item')[0].reset()
   $("#item_id").val("");
   indiceEdicion = null
   deleted_items_ids.length = 0
   $('#b_add').html('<i class="glyphicon glyphicon-plus"></i> Add')
   $('#b_cancelar_item').hide()
}

//Inserta los datos en la tabla de la base de datos
function insertar(data){
  Elaborado.sync().then(() => {
    return Elaborado.create(data, {
      include: [{model: ElaboradoItem,as: 'items'}]
    })
  }).then(data => {
    auditoria.insertarAuditoria("elaborado","Nuevo producto elaborado #"+data.id)
    restauraValores()
    filtrar()
  }, function(err) {
    alert(err.errors[0].message)
  })
}

/*Funcion para actualizar los items
Agrega nuevos, elimina los retirados y autualiza los editados*/
function actualizarItems(cabecera) {
   let promises = []
   promises.push(
      ElaboradoItem.destroy({
         where: {id: deleted_items_ids}
      })
   )
   $.each(items, function (index, value) {
      let promise = new Promise((resolve, reject) => {
         ElaboradoItem.findOrCreate({
            where: {
               id: parseInt(value.id) || null
            },
            defaults: value
         }).spread((item, created) => {
            if (created) {
               cabecera.addItem(item)
               resolve()
            } else {
               item.updateAttributes(value)
                  .then(() => {
                     resolve()
                  })
            }
         })
      })
      promises.push(promise)
   })
   return Promise.all(promises)
}


//Modificar los datos en la tabla de la base de datos
function modificar(data){
  return elaboradoAttr.updateAttributes(data)
  .then(cabecera => {
     actualizarItems(cabecera)
        .then(() => {
    auditoria.insertarAuditoria("elaborado","Modificacion producto elaborado #"+elaboradoAttr.id)
    filtrar()
    restauraValores()
  }, function(err) {
    alert(err.errors[0].message)
    })
  })
}

function eliminar(){
  elaboradoAttr.destroy().then(function(){
  auditoria.insertarAuditoria("elaborado","Eliminacion producto elaborado #"+elaboradoAttr.id)
  restauraValores()
  filtrar()
})
}

function cargarMat_prima(){
  mat_primas=[{"id":"","descripcion":"SELECCIONAR MATERIA PRIMA"}]
  Mat_prima.findAll({
    include:[
      {model:Medida,attributes:["unid_medida"]}
    ]
  }).then(results => {
    results.forEach(function (materia) {
      materia.dataValues.medida = materia.medida.dataValues
      mat_primas.push(materia.dataValues)
    });
    util.buildSelect(mat_primas, 's_mat_prima')
  })
}


function filtrar(){
  let filtro = $.trim($('#buscador').val())
  let elaborados=[]
  if(filtro){
    let condiciones = {
                        descripcion: {$like: '%' + filtro.toUpperCase() + '%'},
                      }
    Elaborado.findAll({
      attributes:['id','descripcion','caracteristica'],
      where: { $or:condiciones },
     }).then(results => {
      results.forEach(function (elaborado) {
        elaborados.push(elaborado.dataValues)
      });
      util.buildTable(elaborados, 't_elaborado')
    })
  }else{
    util.buildTable([], 't_elaborado')
  }
}

//Se recupera por id y se prepara para edicion o eliminacion
function recuperarPorId(id){
  Elaborado.findById(id,
    {include: [
      {model: ElaboradoItem,as: 'items',
        include: [
          {model:Mat_prima,attributes:['descripcion'],
            include:[
              {model:Medida,attributes:["unid_medida"]}
            ]
          },
        ]
      }
    ]}
   ).then(result => {
    prepararParaEdicion(result)
  })
}

function prepararParaEdicion(result){
  restauraValores()
  elaboradoAttr = result
  $.each(result.dataValues.items, function (index, item) {
     let data = {
       "id":item.id,
       "cantidad":item.cantidad,
       "elaboradoId":item.elaboradoId,
       "materiaId":item.materiaId,
       "descripcion":item.materia.descripcion,
       "costo":item.costo,
       "unid_medida":item.materia.medida.unid_medida,
     }
     cargaItem(data)
  })
  util.cargarFormulario(result.dataValues, "f_elaborado")
  $('a[href="#formulario"]').tab('show');
  $('#b_guardar').html('<i class="glyphicon glyphicon-ok-circle"></i> Actualizar');
  $('#b_cancelar').show()
  $('#b_eliminar').show()
}
