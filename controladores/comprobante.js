window.$ = window.jQuery = require('jquery')
const {Comprobante} = require('../modelo/modelos.js')//Modelos
const util = require('../extra/util')//Utilidades
const auditoria = require('./auditoria')

/**************************/
//Libreria css http://getbootstrap.com/css/
//npm install bootstrap
require('bootstrap')
/**************************/

/**************************/
//Libreria para validar formularios http://1000hz.github.io/bootstrap-validator/
//npm install bootstrap-validator
require('bootstrap-validator')
/**************************/

let comprobanteAttr//Atributo utilizado para almacenar la instacia del objeto a ser modificado

util.enterNavigation();//Usar enter para navegar el form

//Manejo de evento submit del formulario
$('#f_comprobante').validator().submit(function( event ) {
  if (!event.isDefaultPrevented()) {
    util.mayus('f_comprobante')
    var data = $('#f_comprobante').serializeJSON({checkboxUncheckedValue: "false"})
    if(!comprobanteAttr){
      insertar(data)
    }else{
      modificar(data)
    }
    event.preventDefault()
  }
})

//Recupera el id del objeto al seleccionar una fila de la tabla
$('#t_comprobante tbody').on('click', 'tr', function () {
    let id = ($(this).find('td').html())
    recuperarPorId(id)
})

//Restaura vista original
$('#b_cancelar').on('click', restauraValores )

$('#b_eliminar').on('click', function () {
  if (confirm('Esta seguro que desa eliminar el registro?')) {
      eliminar()
  }
})

//Restaura vista original
$('#buscador').keyup( function() {
  util.delay(function(){
    filtrar()
  }, 500 )
})

//Restarurar valores por defecto
function restauraValores(){
  $('#b_guardar').html('<i class="glyphicon glyphicon-ok-circle"></i> Guardar')
  $('#b_cancelar').hide()
  $('#b_eliminar').hide()
  comprobanteAttr = null
  $("#f_comprobante")[0].reset()//limpia el formulario
}

//Inserta los datos en la tabla de la base de datos
function insertar(data){
  Comprobante.sync().then(() => {
    return Comprobante.create(data)
  }).then(data => {
    auditoria.insertarAuditoria("comprobante","Nuevo comprobante #"+data.id)
    restauraValores()
    filtrar()
  }, function(err) {
    alert(err.errors[0].message)
  })
}

//Modificar los datos en la tabla de la base de datos
function modificar(data){
  return comprobanteAttr.updateAttributes(data)
  .then(function() {
    auditoria.insertarAuditoria("comprobante","Modificacion de comprobante #"+comprobanteAttr.id)
    filtrar()
    restauraValores()
  }, function(err) {
    alert(err.errors[0].message)
  })
}

function eliminar(){
  comprobanteAttr.destroy().then(function () {
    auditoria.insertarAuditoria("comprobante","Eliminacion de comprobante #"+comprobanteAttr.id)
    restauraValores()
    filtrar()
  })
}
function filtrar(){
  let filtro = $.trim($('#buscador').val())
  let comprobantes=[]
  if(filtro){
    let condiciones = {
                        descripcion: {$like: '%' + filtro.toUpperCase() + '%'},
                      }
    Comprobante.findAll({
      attributes:['id','descripcion','sigla'],
      where: { $or:condiciones }
    }).then(results => {
      results.forEach(function (comprobante) {
        comprobantes.push(comprobante.dataValues)
      });
      util.buildTable(comprobantes, 't_comprobante')
    })
  }else{
    util.buildTable([], 't_comprobante')
  }
}

//Se recupera por id y se prepara para edicion o eliminacion
function recuperarPorId(id){
  Comprobante.findById(id).then(result => {
    prepararParaEdicion(result)
  })
}

function prepararParaEdicion(result){
  restauraValores()
  comprobanteAttr = result
  util.cargarFormulario(result.dataValues,"f_comprobante")
  $('a[href="#formulario"]').tab('show');
  $('#b_guardar').html('<i class="glyphicon glyphicon-ok-circle"></i> Actualizar');
  $('#b_cancelar').show()
  $('#b_eliminar').show()
}
