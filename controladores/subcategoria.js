
window.$ = window.jQuery = require('jquery')
const {Subcategoria,Categoria} = require('../modelo/modelos.js')//Modelos
const util = require('../extra/util')//Utilidades
const auditoria = require('./auditoria')

/**************************/
//Libreria css http://getbootstrap.com/css/
//npm install bootstrap
require('bootstrap')
/**************************/

/**************************/
//Libreria para validar formularios http://1000hz.github.io/bootstrap-validator/
//npm install bootstrap-validator
require('bootstrap-validator')
/**************************/

let subcategoriaAttr//Atributo utilizado para almacenar la instacia del objeto a ser modificado
let categorias

util.enterNavigation();//Usar enter para navegar el form

cargarCategorias()

//Manejo de evento submit del formulario
$('#f_subcategoria').validator().submit(function( event ) {
  if (!event.isDefaultPrevented()) {
    util.mayus('f_subcategoria')
    var data = $('#f_subcategoria').serializeJSON({checkboxUncheckedValue: "false"})
    if(!subcategoriaAttr){
      insertar(data)
    }else{
      modificar(data)
    }
    event.preventDefault()
  }
})

//Recupera el id del objeto al seleccionar una fila de la tabla
$('#t_subcategoria tbody').on('click', 'tr', function () {
    let id = ($(this).find('td').html())
    recuperarPorId(id)
})

//Restaura vista original
$('#b_cancelar').on('click', restauraValores )

$('#b_eliminar').on('click', function () {
  if (confirm('Esta seguro que desea eliminar el registro?')) {
      eliminar()
  }
})

//Restaura vista original
$('#buscador').keyup( function() {
  util.delay(function(){
    filtrar()
  }, 500 )
})

//Restarurar valores por defecto
function restauraValores(){
  $('#b_guardar').html('<i class="glyphicon glyphicon-ok-circle"></i> Guardar')
  $('#b_cancelar').hide()
  $('#b_eliminar').hide()
  subcategoriaAttr = null
  $("#f_subcategoria")[0].reset()//limpia el formulario
}

//Inserta los datos en la tabla de la base de datos
function insertar(data){
    if (data.categoriaId === "") data.categoriaId= null
    Subcategoria.sync().then(() => {
    return Subcategoria.create(data)
  }).then(data => {
    auditoria.insertarAuditoria("subcategoria","Nueva subcategoria #"+data.id)
    restauraValores()
    filtrar()
  }, function(err) {
    alert(err.errors[0].message)
  })
}

//Modificar los datos en la tabla de la base de datos
function modificar(data){
  if (data.categoriaId === "") data.categoriaId= null
  return subcategoriaAttr.updateAttributes(data)
  .then(function() {
    auditoria.insertarAuditoria("subcategoria ","Modificacion de subcategoria #"+subcategoriaAttr.id)
    filtrar()
    restauraValores()
  }, function(err) {
    alert(err.errors[0].message)
  })
}

function eliminar(){
  subcategoriaAttr.destroy().then(function () {
    auditoria.insertarAuditoria("subcategoria","Eliminacion de subcategoria #"+subcategoriaAttr.id)
    restauraValores()
    filtrar()
  })
}

function cargarCategorias(){
  categorias=[{"id":"","descripcion":"SELECCIONAR CATEGORIA"}]
  Categoria.findAll().then(results => {
    results.forEach(function (categoria) {
      categorias.push(categoria.dataValues)
    });
    util.buildSelect(categorias, 's_categoria')
  })
}

function getTipoById(id){
  Categoria.findById(id).then(result => {
    return result
  })
}

function filtrar(){
  let filtro = $.trim($('#buscador').val())
  let subcategorias=[]
  if(filtro){
    let condiciones = {
                        descripcion: {$like: '%' + filtro.toUpperCase() + '%'},
                      }
    Subcategoria.findAll({
      attributes:['id','descripcion'],
      where: { $or:condiciones },
      include: [{model:Categoria,attributes:['descripcion']}]
    }).then(results => {
      results.forEach(function (subcategoria) {
        subcategorias.push({
          'id' : subcategoria.id,
          'descripcion' : subcategoria.descripcion,
          'categoria' : (subcategoria.categoria)? subcategoria.categoria.descripcion:""
        })
      });
      util.buildTable(subcategorias, 't_subcategoria')
    })
  }else{
    util.buildTable([], 't_subcategoria')
  }
}

//Se recupera por id y se prepara para edicion o eliminacion
function recuperarPorId(id){
  Subcategoria.findById(id).then(result => {
    prepararParaEdicion(result)
  })
}

function prepararParaEdicion(result){
  restauraValores()
  subcategoriaAttr = result
  util.cargarFormulario(result.dataValues,"f_subcategoria")
  $('a[href="#formulario"]').tab('show');
  $('#b_guardar').html('<i class="glyphicon glyphicon-ok-circle"></i> Actualizar');
  $('#b_cancelar').show()
  $('#b_eliminar').show()
}
