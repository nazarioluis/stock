window.$ = window.jQuery = require('jquery')
const {Producto,Categoria,Subcategoria,Marca,Medida} = require('../modelo/modelos.js')//Modelos
const util = require('../extra/util')//Utilidades
const auditoria = require('./auditoria')

/**************************/
//Libreria css http://getbootstrap.com/css/
//npm install bootstrap
require('bootstrap')
/**************************/

/**************************/
//Libreria para validar formularios http://1000hz.github.io/bootstrap-validator/
//npm install bootstrap-validator
require('bootstrap-validator')
/**************************/

let productoAttr//Atributo utilizado para almacenar la instacia del objeto a ser modificado
let categoria
let subcategoria
let marca
let medida

util.enterNavigation();//Usar enter para navegar el form

cargarCategorias()
cargarMarcas()
cargarUnidadesMediada()

//Manejo de evento submit del formulario
$('#f_producto').validator().submit(function( event ) {
  if (!event.isDefaultPrevented()) {
    util.mayus('f_producto')
    var data = $('#f_producto').serializeJSON({checkboxUncheckedValue: "false"})
    if(!productoAttr){
      insertar(data)
    }else{
      modificar(data)
    }
    event.preventDefault()
  }
})

//Recupera el id del objeto al seleccionar una fila de la tabla
$('#t_producto tbody').on('click', 'tr', function () {
    let id = ($(this).find('td').html())
    recuperarPorId(id)
})

//Restaura vista original
$('#b_cancelar').on('click', restauraValores )

$('#b_eliminar').on('click', function () {
  if(confirm('Esta seguro que desea eliminar el producto:\n['+productoAttr.id+'] - '+productoAttr.descripcion+'?')){
      eliminar()
  }
})

/
//Restaura vista original
$('#buscador').keyup( function() {
  util.delay(function(){
    filtrar()
  }, 500 )
})

/**Evento en combobox tipo para buscar los
servicios vinculados con el tipo seleccionado*/
$("#s_categoria").change(function() {
  let categoriaId = $('#s_categoria option:selected').val()
  cargarSubcategorias(categoriaId)
})

//Restarurar valores por defecto
function restauraValores(){
  $('#b_guardar').html('<i class="glyphicon glyphicon-ok-circle"></i> Guardar')
  $('#b_cancelar').hide()
  $('#b_eliminar').hide()
  productoAttr = null
  $("#f_producto")[0].reset()//limpia el formulario
  $("#estado").attr("checked",true)
}

//Inserta los datos en la tabla de la base de datos
function insertar(data){
  if (data.marcaId === "") data.marcaId= null
  if (data.categoriaId === "") data.categoriaId= null
  if (data.subcategoriaId === "") data.subcategoriaId= null
  if (data.medidaId === "") data.medidaId= null
  Producto.sync().then(() => {
    return Producto.create(data)
  }).then(data => {
    auditoria.insertarAuditoria("producto","Nuevo producto #"+data.id)
    restauraValores()
    filtrar()
  }, function(err) {
    alert(err.errors[0].message)
  })
}

//Modificar los datos en la tabla de la base de datos
function modificar(data){
  if (data.marcaId === "") data.marcaId= null
  if (data.categoriaId === "") data.categoriaId= null
  if (data.subcategoriaId === "") data.subcategoriaId= null
  if (data.medidaId === "") data.medidaId= null
  return productoAttr.updateAttributes(data)
  .then(function() {
    auditoria.insertarAuditoria("producto","Modificacion de producto #"+productoAttr.id)
    filtrar()
    restauraValores()
  }, function(err) {
    alert(err.errors[0].message)
  })
}

function eliminar(){
  productoAttr.destroy().then(function () {
    auditoria.insertarAuditoria("producto","Eliminacion de producto #"+productoAttr.id)
    restauraValores()
    filtrar()
  })
}

function cargarMarcas(){
  marcas=[{"id":"","descripcion":"SELECCIONAR MARCAS"}]
  Marca.findAll().then(results => {
    results.forEach(function (marca) {
      marcas.push(marca.dataValues)
    });
    util.buildSelect(marcas, 's_marca')
  })
}

function cargarCategorias(){
  categorias=[{"id":"","descripcion":"SELECCIONAR CATEGORIA"}]
  Categoria.findAll().then(results => {
    results.forEach(function (categoria) {
      categorias.push(categoria.dataValues)
    });
    util.buildSelect(categorias, 's_categoria')
    let subcategorias=[{"id":"","descripcion":"SELECCIONAR SUBCATEGORIA"}]
    util.buildSelect(subcategorias, 's_subcategoria')
  })
}


/**Evento en combobox tipo para buscar los
servicios vinculados con el tipo seleccionado*/
$("#s_categoria").change(function() {
  let categoriaId = $('#s_categoria option:selected').val()
  cargarSubcategorias(categoriaId)
})

function cargarSubcategorias(categoriaId){
  return new Promise((resolve, reject) => {
  let subcategorias=[{"id":"","descripcion":"SELECCIONAR SUBCATEGORIA"}]
  Subcategoria.findAll({
      where:{categoriaId: categoriaId}
  }).then(results => {
    results.forEach(function (subcategoria) {
      subcategorias.push(subcategoria.dataValues)
    })
    util.buildSelect(subcategorias, 's_subcategoria')
    resolve()
  }).catch(err => {
    util.buildSelect(subcategorias, 's_subcategoria')
    })
  })
}

function cargarUnidadesMediada(){
  medidas=[{"id":"","descripcion":"SELECCIONAR UNIDAD DE MEDIDA"}]
  Medida.findAll().then(results => {
    results.forEach(function (medida) {
      medidas.push(medida.dataValues)
    });
    util.buildSelect(medidas, 's_medida')
  })
}

function filtrar(){
  let filtro = $.trim($('#buscador').val())
  let productos=[]
  if(filtro){
    let condiciones = {
                        descripcion: {$like: '%' + filtro.toUpperCase() + '%'},
                      }
    Producto.findAll({
      attributes:['id','descripcion','preunitario','sminunid','sreal'],
      where: { $or:condiciones },
      include: [{model:Medida,attributes:['descripcion']}]
    }).then(results => {
      results.forEach(function (producto) {
        productos.push({
          id: producto.id,
          descripcion: producto.descripcion,
          preunitario: producto.preunitario,
          unidmedida: producto.medida.descripcion,
          sminunid: producto.sminunid,
          sreal: producto.sreal
        })
      });
      util.buildTable(productos, 't_producto')
    })
  }else{
    util.buildTable([], 't_producto')
  }
}

//Se recupera por id y se prepara para edicion o eliminacion
function recuperarPorId(id){
  Producto.findById(id,
    {include:[
      {model:Marca,attributes:['id','descripcion']},
      {model:Categoria,attributes:['id','descripcion']},
      {model:Subcategoria,attributes:['id','descripcion']},
      {model:Medida,attributes:['id','descripcion']},
    ]}
  ).then(result => {
    prepararParaEdicion(result)
  })
}

function recuperarPorDescripcion(descripcion){
  Producto.findOne({where:{'descripcion':descripcion}}).then(result => {
    if (result) prepararParaEdicion(result)
  })
}

function prepararParaEdicion(result){
  restauraValores()
  productoAttr = result
  let categoriaId = result.dataValues.categoriaId;
  cargarSubcategorias(categoriaId).then(function(){
  util.cargarFormulario(result.dataValues,"f_producto")
  })
  $('a[href="#formulario"]').tab('show');
  $('#b_guardar').html('<i class="glyphicon glyphicon-ok-circle"></i> Actualizar');
  $('#b_cancelar').show()
  $('#b_eliminar').show()
}
