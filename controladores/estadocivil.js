window.$ = window.jQuery = require('jquery')
const {Estadocivil} = require('../modelo/modelos.js')//Modelos
const util = require('../extra/util')//Utilidades
const auditoria = require('./auditoria')

/**************************/
//Libreria css http://getbootstrap.com/css/
//npm install bootstrap
require('bootstrap')
/**************************/

/**************************/
//Libreria para validar formularios http://1000hz.github.io/bootstrap-validator/
//npm install bootstrap-validator
require('bootstrap-validator')
/**************************/

let estadocivilAttr//Atributo utilizado para almacenar la instacia del objeto a ser modificado

util.enterNavigation();//Usar enter para navegar el form

//Manejo de evento submit del formulario
$('#f_estadocivil').validator().submit(function( event ) {
  if (!event.isDefaultPrevented()) {
    util.mayus('f_estadocivil')
    var data = $('#f_estadocivil').serializeJSON({checkboxUncheckedValue: "false"})
    if(!estadocivilAttr){
      insertar(data)
    }else{
      modificar(data)
    }
    event.preventDefault()
  }
})

//Recupera el id del objeto al seleccionar una fila de la tabla
$('#t_estadocivil tbody').on('click', 'tr', function () {
    let id = ($(this).find('td').html())
    recuperarPorId(id)
})

//Restaura vista original
$('#b_cancelar').on('click', restauraValores )

$('#b_eliminar').on('click', function () {
  if (confirm('Esta seguro que desa eliminar el registro?')) {
      eliminar()
  }
})

//Restaura vista original
$('#buscador').keyup( function() {
  util.delay(function(){
    filtrar()
  }, 500 )
})

//Restarurar valores por defecto
function restauraValores(){
  $('#b_guardar').html('<i class="glyphicon glyphicon-ok-circle"></i> Guardar')
  $('#b_cancelar').hide()
  $('#b_eliminar').hide()
  estadocivilAttr = null
  $("#f_estadocivil")[0].reset()//limpia el formulario
}

//Inserta los datos en la tabla de la base de datos
function insertar(data){
  Estadocivil.sync().then(() => {
    return Estadocivil.create(data)
  }).then(data => {
    auditoria.insertarAuditoria("estadocivil","Nuevo estado civil #"+data.id)
    restauraValores()
    filtrar()
  }, function(err) {
    alert(err.errors[0].message)
  })
}

//Modificar los datos en la tabla de la base de datos
function modificar(data){
  return estadocivilAttr.updateAttributes(data)
  .then(function() {
    auditoria.insertarAuditoria("estadocivil","Modificacion de estado civil #"+estadocivilAttr.id)
    filtrar()
    restauraValores()
  }, function(err) {
    alert(err.errors[0].message)
  })
}

function eliminar(){
  estadocivilAttr.destroy().then(function(){
  auditoria.insertarAuditoria("estadocivil","Eliminacion de estado civil #"+estadocivilAttr.id)
  restauraValores()
  filtrar()
})
}

function filtrar(){
  let filtro = $.trim($('#buscador').val())
  let estadociviles=[]
  if(filtro){
    let condiciones = {
                        descripcion: {$like: '%' + filtro.toUpperCase() + '%'},
                      }
    Estadocivil.findAll({
      attributes:['id','descripcion'],
      where: { $or:condiciones }
    }).then(results => {
      results.forEach(function (estadocivil) {
        estadociviles.push(estadocivil.dataValues)
      });
      util.buildTable(estadociviles, 't_estadocivil')
    })
  }else{
    util.buildTable([], 't_estadocivil')
  }
}

//Se recupera por id y se prepara para edicion o eliminacion
function recuperarPorId(id){
  Estadocivil.findById(id).then(result => {
    prepararParaEdicion(result)
  })
}

function prepararParaEdicion(result){
  restauraValores()
  estadocivilAttr = result
  util.cargarFormulario(result.dataValues,"f_estadocivil")
  $('a[href="#formulario"]').tab('show');
  $('#b_guardar').html('<i class="glyphicon glyphicon-ok-circle"></i> Actualizar');
  $('#b_cancelar').show()
  $('#b_eliminar').show()
}
