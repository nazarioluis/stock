window.$ = window.jQuery = require('jquery')
const {Mat_prima,Medida} = require('../modelo/modelos.js')//Modelos
const util = require('../extra/util')//Utilidades
const auditoria = require('./auditoria')

/**************************/
//Libreria css http://getbootstrap.com/css/
//npm install bootstrap
require('bootstrap')
/**************************/

/**************************/
//Libreria para validar formularios http://1000hz.github.io/bootstrap-validator/
//npm install bootstrap-validator
require('bootstrap-validator')
/**************************/

let mat_primaAttr//Atributo utilizado para almacenar la instacia del objeto a ser modificado
let medida

util.enterNavigation();//Usar enter para navegar el form

cargarUnidadesMediada()

//Manejo de evento submit del formulario
$('#f_matprima').validator().submit(function( event ) {
  if (!event.isDefaultPrevented()) {
    util.mayus('f_matprima')
    var data = $('#f_matprima').serializeJSON({checkboxUncheckedValue: "false"})
    if(!mat_primaAttr){
      insertar(data)
    }else{
      modificar(data)
    }
    event.preventDefault()
  }
})

//Recupera el id del objeto al seleccionar una fila de la tabla
$('#t_matprima tbody').on('click', 'tr', function () {
    let id = ($(this).find('td').html())
    recuperarPorId(id)
})

//Restaura vista original
$('#b_cancelar').on('click', restauraValores )

$('#b_eliminar').on('click', function () {
  if (confirm('Esta seguro que desa eliminar el registro?')) {
      eliminar()
  }
})

//Restaura vista original
$('#buscador').keyup( function() {
  util.delay(function(){
    filtrar()
  }, 500 )
})

//Restarurar valores por defecto
function restauraValores(){
  $('#b_guardar').html('<i class="glyphicon glyphicon-ok-circle"></i> Guardar')
  $('#b_cancelar').hide()
  $('#b_eliminar').hide()
  mat_primaAttr = null
  $("#f_matprima")[0].reset()//limpia el formulario
}

//Inserta los datos en la tabla de la base de datos
function insertar(data){
  if (data.medidaId === "") data.medidaId= null
  Mat_prima.sync().then(() => {
    return Mat_prima.create(data)
  }).then(data => {
    auditoria.insertarAuditoria("mat_prima","Nueva materia prima #"+data.id)
    restauraValores()
    filtrar()
  }, function(err) {
    alert(err.errors[0].message)
  })
}

//Modificar los datos en la tabla de la base de datos
function modificar(data){
  if (data.medidaId === "") data.medidaId= null
  return mat_primaAttr.updateAttributes(data)
  .then(function() {
    auditoria.insertarAuditoria("mat_prima","Modificacion de materia prima #"+mat_primaAttr.id)
    filtrar()
    restauraValores()
  }, function(err) {
    alert(err.errors[0].message)
  })
}

function eliminar(){
  mat_primaAttr.destroy().then(function () {
    auditoria.insertarAuditoria("mat_prima","Eliminacion de materia prima #"+mat_primaAttr.id)
    restauraValores()
    filtrar()
  })
}

function cargarUnidadesMediada(){
  medidas=[{"id":"","descripcion":"SELECCIONAR UNIDAD DE MEDIDA"}]
  Medida.findAll().then(results => {
    results.forEach(function (medida) {
      medidas.push(medida.dataValues)
    });
    util.buildSelect(medidas, 's_medida')
  })
}

function filtrar(){
  let filtro = $.trim($('#buscador').val())
  let matprimas=[]
  if(filtro){
    let condiciones = {
                        descripcion: {$like: '%' + filtro.toUpperCase() + '%'},
                      }
    Mat_prima.findAll({
      attributes:['id','descripcion','precomprapro','sminimo','sreal'],
      where: { $or:condiciones },
      include: [{model:Medida,attributes:['descripcion']}]
    }).then(results => {
      results.forEach(function (mat_prima) {
        matprimas.push({
          id: mat_prima.id,
          descripcion: mat_prima.descripcion,
          precomprapro: mat_prima.precomprapro,
          unidmedida: mat_prima.medida.descripcion,
          sminimo: mat_prima.sminimo,
          sreal: mat_prima.sreal
        })
      });
      util.buildTable(matprimas, 't_matprima')
    })
  }else{
    util.buildTable([], 't_matprima')
  }
}

//Se recupera por id y se prepara para edicion o eliminacion
function recuperarPorId(id){
  Mat_prima.findById(id).then(result => {
    prepararParaEdicion(result)
  })
}

function prepararParaEdicion(result){
  restauraValores()
  mat_primaAttr = result
  util.cargarFormulario(result.dataValues,"f_matprima")
  $('a[href="#formulario"]').tab('show');
  $('#b_guardar').html('<i class="glyphicon glyphicon-ok-circle"></i> Actualizar');
  $('#b_cancelar').show()
  $('#b_eliminar').show()
}
