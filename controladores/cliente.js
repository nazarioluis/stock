window.$ = window.jQuery = require('jquery')
const {Cliente,Ciudad,Sexo,Estadocivil} = require('../modelo/modelos.js')//Modelos
const util = require('../extra/util')//Utilidades
const auditoria = require('./auditoria')

require('dateonly');

/**************************/
//Libreria css http://getbootstrap.com/css/
//npm install bootstrap
require('bootstrap')
/**************************/

/**************************/
//Libreria para validar formularios http://1000hz.github.io/bootstrap-validator/
//npm install bootstrap-validator
require('bootstrap-validator')
/**************************/

let clienteAttr//Atributo utilizado para almacenar la instacia del objeto a ser modificado
let ciudad
let sexo
let estadocivil

util.enterNavigation();//Usar enter para navegar el form

cargarCiudad()
cargarSexo()
cargarEstadocivil()

let fechaActual = new Date() //fecha el sistema
$('#fechanacimiento').val(fechaActual.toISOString().split('T')[0])

//Manejo de evento submit del formulario
$('#f_cliente').validator().submit(function( event ) {
  if (!event.isDefaultPrevented()) {
    util.mayus('f_cliente')
    var data = $('#f_cliente').serializeJSON({checkboxUncheckedValue: "false"})
    if(!clienteAttr){
      insertar(data)
    }else{
      modificar(data)
    }
    event.preventDefault()
  }
})

//Recupera el id del objeto al seleccionar una fila de la tabla
$('#t_cliente tbody').on('click', 'tr', function () {
    let id = ($(this).find('td').html())
    recuperarPorId(id)
})

//Restaura vista original
$('#b_cancelar').on('click', restauraValores )

$('#b_eliminar').on('click', function () {
  if (confirm('Esta seguro que desa eliminar el registro?')) {
      eliminar()
  }
})

//Consultar por cedula
$('#documento').keydown(function(e){
    if(e.keyCode == 120 || e.keyCode == 13 || e.keyCode == 9)//f9 o enter o tabulador
    {
      let documento = $('#documento').val()
      recuperarPorDocumento(documento)
    }
});

//Restaura vista original
$('#buscador').keyup( function() {
  util.delay(function(){
    filtrar()
  }, 500 )
})

//Restarurar valores por defecto
function restauraValores(){
  $('#b_guardar').html('<i class="glyphicon glyphicon-ok-circle"></i> Guardar')
  $('#b_cancelar').hide()
  $('#b_eliminar').hide()
  clienteAttr = null
  $("#f_cliente")[0].reset()//limpia el formulario
  // se pasa la fecha actual a formato yyyy/mm/dd para que muestre en el input type date
  $('#fechanacimiento').val(fechaActual.toISOString().split('T')[0])
}

//Inserta los datos en la tabla de la base de datos
function insertar(data){
    if (data.ciudadId === "") data.ciudadId= null
    if (data.estadocivilId === "") data.estadocivilId= null
  Cliente.sync().then(() => {
    return Cliente.create(data)
  }).then(data => {
    auditoria.insertarAuditoria("cliente","Nuevo cliente #"+data.id)
    restauraValores()
    filtrar()
  }, function(err) {
    console.log(err);
    alert(err.errors[0].message)
  })
}

//Modificar los datos en la tabla de la base de datos
function modificar(data){
  if (data.ciudadId === "") data.ciudadId= null
  if (data.estadocivilId === "") data.estadocivilId= null
  return clienteAttr.updateAttributes(data)
  .then(function() {
    auditoria.insertarAuditoria("cliente","Modificacion de cliente #"+clienteAttr.id)
    filtrar()
    restauraValores()
  }, function(err) {
    alert(err.errors[0].message)
  })
}

function eliminar(){
  clienteAttr.destroy().then(function () {
  auditoria.insertarAuditoria("cliente","Eliminacion de cliente #"+clienteAttr.id)
  restauraValores()
  filtrar()
})
}

function cargarCiudad(){
  ciudades=[{"id":"","descripcion":"SELECCIONAR CIUDAD"}]
  Ciudad.findAll().then(results => {
    results.forEach(function (ciudad) {
      ciudades.push(ciudad.dataValues)
    });
    util.buildSelect(ciudades, 's_ciudad')
  })
}

function cargarEstadocivil(){
  estadociviles=[{"id":"","descripcion":"SELECCIONAR ESTADO CIVIL"}]
  Estadocivil.findAll().then(results => {
    results.forEach(function (estadocivil) {
      estadociviles.push(estadocivil.dataValues)
    });
    util.buildSelect(estadociviles, 's_estadocivil')
  })
}

function cargarSexo(){
  sexos=[
    {"id":"","descripcion":"SELECCIONAR SEXO"},
    {"id":"M","descripcion":"MASCULINO"},
    {"id":"F","descripcion":"FEMENINO"},
  ]
  util.buildSelect(sexos, 's_sexo')
}

function filtrar(){
  let filtro = $.trim($('#buscador').val())
  let clientes=[]
  if(filtro){
    let condiciones = {
                        nombre: {$like: '%' + filtro.toUpperCase() + '%'},
                        documento: {$like: '%' + filtro + '%'}
                      }
    Cliente.findAll({
      attributes:['id','nombre','documento','direccion','email','telefono1','telefono2'],
      where: { $or:condiciones }
    }).then(results => {
      results.forEach(function (cliente) {
        clientes.push(cliente.dataValues)
      });
      util.buildTable(clientes, 't_cliente')
    })
  }else{
    util.buildTable([], 't_cliente')
  }
}

//Se recupera por id y se prepara para edicion o eliminacion
function recuperarPorId(id){
  Cliente.findById(id).then(result => {
    prepararParaEdicion(result)
  })
}

function recuperarPorDocumento(documento){
  Cliente.findOne({where:{'documento':documento}}).then(result => {
    if (result) prepararParaEdicion(result)
  })
}

function prepararParaEdicion(result){
  restauraValores()
  clienteAttr = result
  util.cargarFormulario(result.dataValues,"f_cliente")
  // se pasa la fecha a formato yyyy/mm/dd para que muestre en el input type date despues del util.cargar formulario
  $('#fechanacimiento').val(result.dataValues.fechanacimiento.toISOString().split('T')[0])
  $('a[href="#formulario"]').tab('show');
  $('#b_guardar').html('<i class="glyphicon glyphicon-ok-circle"></i> Actualizar');
  $('#b_cancelar').show()
  $('#b_eliminar').show()
}
