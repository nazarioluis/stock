window.$ = window.jQuery = require('jquery')
const {Impuesto} = require('../modelo/modelos.js')//Modelos
const util = require('../extra/util')//Utilidades

/**************************/
//Libreria css http://getbootstrap.com/css/
//npm install bootstrap
require('bootstrap')
/**************************/

/**************************/
//Libreria para validar formularios http://1000hz.github.io/bootstrap-validator/
//npm install bootstrap-validator
require('bootstrap-validator')
/**************************/

let impuestoAttr//Atributo utilizado para almacenar la instacia del objeto a ser modificado

util.enterNavigation();//Usar enter para navegar el form

//Manejo de evento submit del formulario
$('#f_impuesto').validator().submit(function( event ) {
  if (!event.isDefaultPrevented()) {
    util.mayus('f_impuesto')
    var data = $('#f_impuesto').serializeJSON({checkboxUncheckedValue: "false"})
    if(!impuestoAttr){
      insertar(data)
    }else{
      modificar(data)
    }
    event.preventDefault()
  }
})

//Recupera el id del objeto al seleccionar una fila de la tabla
$('#t_impuesto tbody').on('click', 'tr', function () {
    let id = ($(this).find('td').html())
    recuperarPorId(id)
})

//Restaura vista original
$('#b_cancelar').on('click', restauraValores )

$('#b_eliminar').on('click', function () {
  if (confirm('Esta seguro que desa eliminar el registro?')) {
      eliminar()
  }
})

//Restaura vista original
$('#buscador').keyup( function() {
  util.delay(function(){
    filtrar()
  }, 500 )
})

//Restarurar valores por defecto
function restauraValores(){
  $('#b_guardar').html('<i class="glyphicon glyphicon-ok-circle"></i> Guardar')
  $('#b_cancelar').hide()
  $('#b_eliminar').hide()
  impuestoAttr = null
  $("#f_impuesto")[0].reset()//limpia el formulario
}

//Inserta los datos en la tabla de la base de datos
function insertar(data){
  Impuesto.sync().then(() => {
    return Impuesto.create(data)
  }).then(function() {
    restauraValores()
    filtrar()
  }, function(err) {
    alert(err.errors[0].message)
  })
}

//Modificar los datos en la tabla de la base de datos
function modificar(data){
  return impuestoAttr.updateAttributes(data)
  .then(function() {
    filtrar()
    restauraValores()
  }, function(err) {
    alert(err.errors[0].message)
  })
}

function eliminar(){
  impuestoAttr.destroy()
  restauraValores()
  filtrar()
}

function filtrar(){
  let filtro = $.trim($('#buscador').val())
  let impuestos=[]
  if(filtro){
    let condiciones = {
                        descripcion: {$like: '%' + filtro.toUpperCase() + '%'},
                      }
    Impuesto.findAll({
      attributes:['id','descripcion'],
      where: { $or:condiciones }
    }).then(results => {
      results.forEach(function (impuesto) {
        impuestos.push(impuesto.dataValues)
      });
      util.buildTable(impuestos, 't_impuesto')
    })
  }else{
    util.buildTable([], 't_impuesto')
  }
}

//Se recupera por id y se prepara para edicion o eliminacion
function recuperarPorId(id){
  Impuesto.findById(id).then(result => {
    prepararParaEdicion(result)
  })
}

function prepararParaEdicion(result){
  restauraValores()
  impuestoAttr = result
  util.cargarFormulario(result.dataValues,"f_impuesto")
  $('a[href="#formulario"]').tab('show');
  $('#b_guardar').html('<i class="glyphicon glyphicon-ok-circle"></i> Actualizar');
  $('#b_cancelar').show()
  $('#b_eliminar').show()
}
