window.$ = window.jQuery = require('jquery')
const {Procedencia} = require('../modelo/modelos.js')//Modelos
const util = require('../extra/util')//Utilidades
const auditoria = require('./auditoria')

/**************************/
//Libreria css http://getbootstrap.com/css/
//npm install bootstrap
require('bootstrap')
/**************************/

/**************************/
//Libreria para validar formularios http://1000hz.github.io/bootstrap-validator/
//npm install bootstrap-validator
require('bootstrap-validator')
/**************************/

let procedenciaAttr//Atributo utilizado para almacenar la instacia del objeto a ser modificado

util.enterNavigation();//Usar enter para navegar el form

//Manejo de evento submit del formulario
$('#f_procedencia').validator().submit(function( event ) {
  if (!event.isDefaultPrevented()) {
    util.mayus('f_procedencia')
    var data = $('#f_procedencia').serializeJSON({checkboxUncheckedValue: "false"})
    if(!procedenciaAttr){
      insertar(data)
    }else{
      modificar(data)
    }
    event.preventDefault()
  }
})

//Recupera el id del objeto al seleccionar una fila de la tabla
$('#t_procedencia tbody').on('click', 'tr', function () {
    let id = ($(this).find('td').html())
    recuperarPorId(id)
})

//Restaura vista original
$('#b_cancelar').on('click', restauraValores )

$('#b_eliminar').on('click', function () {
  if (confirm('Esta seguro que desa eliminar el registro?')) {
      eliminar()
  }
})

//Restaura vista original
$('#buscador').keyup( function() {
  util.delay(function(){
    filtrar()
  }, 500 )
})

//Restarurar valores por defecto
function restauraValores(){
  $('#b_guardar').html('<i class="glyphicon glyphicon-ok-circle"></i> Guardar')
  $('#b_cancelar').hide()
  $('#b_eliminar').hide()
  procedenciaAttr = null
  $("#f_procedencia")[0].reset()//limpia el formulario
}

//Inserta los datos en la tabla de la base de datos
function insertar(data){
  Procedencia.sync().then(() => {
    return Procedencia.create(data)
  }).then(data => {
    auditoria.insertarAuditoria("procedencia","Nueva procedencia #"+data.id)
    restauraValores()
    filtrar()
  }, function(err) {
    alert(err.errors[0].message)
  })
}

//Modificar los datos en la tabla de la base de datos
function modificar(data){
  return procedenciaAttr.updateAttributes(data)
  .then(function() {
    auditoria.insertarAuditoria("procedencia","Modificacion de procedencia #"+procedenciaAttr.id)
    filtrar()
    restauraValores()
  }, function(err) {
    alert(err.errors[0].message)
  })
}

function eliminar(){
  procedenciaAttr.destroy().then(function () {
    auditoria.insertarAuditoria("procedencia","Eliminacion de procedencia #"+procedenciaAttr.id)
    restauraValores()
    filtrar()
  })
}

function filtrar(){
  let filtro = $.trim($('#buscador').val())
  let procedencias=[]
  if(filtro){
    let condiciones = {
                        descripcion: {$like: '%' + filtro.toUpperCase() + '%'},
                      }
    Procedencia.findAll({
      attributes:['id','descripcion'],
      where: { $or:condiciones }
    }).then(results => {
      results.forEach(function (procedencia) {
        procedencias.push(procedencia.dataValues)
      });
      util.buildTable(procedencias, 't_procedencia')
    })
  }else{
    util.buildTable([], 't_procedencia')
  }
}

//Se recupera por id y se prepara para edicion o eliminacion
function recuperarPorId(id){
  Procedencia.findById(id).then(result => {
    prepararParaEdicion(result)
  })
}

function prepararParaEdicion(result){
  restauraValores()
  procedenciaAttr = result
  util.cargarFormulario(result.dataValues,"f_procedencia")
  $('a[href="#formulario"]').tab('show');
  $('#b_guardar').html('<i class="glyphicon glyphicon-ok-circle"></i> Actualizar');
  $('#b_cancelar').show()
  $('#b_eliminar').show()
}
