const Sequelize = require('sequelize')
const sequelize = new Sequelize('postgres://postgres:postgres@127.0.0.1:5432/hamb_angelos');

//IsUnique personalizado
require('../extra/sequelizeUnique')(Sequelize)

//CONFIGURACIONES
exports.Funcionario = sequelize.define('funcionario', {
   nombre: {type: Sequelize.STRING,allowNull: false,comment: 'Nombre y Apellido'},
   documento: {type: Sequelize.STRING,allowNull: false},
   email: {type: Sequelize.STRING},
   telefono: {type: Sequelize.STRING},
   cargo: {type: Sequelize.STRING},
   salario: {type: Sequelize.DOUBLE},
   estado: {type: Sequelize.BOOLEAN,comment: "1=true=activo, 0=false=inactivo"}
}, {
   validate: {
      isUnique: sequelize.validateIsUnique(['documento'],
         "'No se pudo realizar la accion! " +
         "Ya existe un funcionario con el número de documento '+self.documento")
   }
});

exports.Rol =  sequelize.define("roles", {
   descripcion: {type: Sequelize.STRING(25),allowNull:false},
   accesos: {type: Sequelize.JSONB,allowNull: false,comment: "Guarda los niveles de acceso en formato JSONB"
   }
},{
   validate: {
      isUnique: sequelize.validateIsUnique(['descripcion'],
         "'Ya existe el rol '+self.descripcion")
   }
})

exports.Usuario = sequelize.define("usuarios",{
   nick:{type: Sequelize.STRING(20),allowNull: false},
   pass:{type: Sequelize.STRING(),allowNull: false},
   estado: {type: Sequelize.STRING(4),allowNull: false,defaultValue: "ACTI",comment: "ACTI=activo, INAC=inactivo"}
},{
   validate: {
      isUnique: sequelize.validateIsUnique(['nick'],
         "'Ya existe el usuario '+self.nick")
   }
})
this.Usuario.belongsTo(this.Rol,{onDelete:"restrict"});
this.Usuario.belongsTo(this.Funcionario,{onDelete:"restrict"});

//OPCIONES ESPECIALES
exports.Auditoria =  sequelize.define("auditorias", {
   moduloId: {type: Sequelize.STRING(20),allowNull:false
   },
   accion: {type: Sequelize.STRING(40),allowNull: false,
   }
})
this.Auditoria.belongsTo(this.Usuario,{onDelete:"restrict"});


//Define estado civil
exports.Estadocivil = sequelize.define('estadocivil', {
  descripcion: {type: Sequelize.STRING,allowNull: false, comment: 'Descripción'}
},{
  validate: {
    isUnique: sequelize.validateIsUnique(['descripcion'],
    "'No se pudo realizar la accion! Ya existe el estado civil '+self.descripcion")
  }
});

//Define departamentos
exports.Departamento = sequelize.define('departamento', {
  descripcion: {type: Sequelize.STRING,allowNull: false, comment: 'Descripción'}
},{
  validate: {
    isUnique: sequelize.validateIsUnique(['descripcion'],
    "'No se pudo realizar la accion! Ya existe el departamento '+self.descripcion")
  }
});

//Define ciudades
exports.Ciudad = sequelize.define('ciudad', {
  descripcion: {type: Sequelize.STRING,allowNull: false, comment: 'Descripción'}
},{
  validate: {
    isUnique: sequelize.validateIsUnique(['descripcion'],
    "'No se pudo realizar la accion! "+
    "Ya existe la ciudad '+self.descripcion+"+"'para el departamento seleccionado'")
  }
});
this.Ciudad.belongsTo(this.Departamento,{onDelete:"restrict"});

// Cliente
exports.Cliente = sequelize.define('cliente', {
  nombre: {type: Sequelize.STRING},
  apodo: {type: Sequelize.STRING},
  documento: {type: Sequelize.STRING},
  direccion: {type: Sequelize.STRING},
  sexo: {type: Sequelize.STRING(1)},
  email: {type: Sequelize.STRING},
  web: {type: Sequelize.STRING},
  fechanacimiento: {type: Sequelize.STRING},
  telefono1: {type: Sequelize.STRING},
  telefono2: {type: Sequelize.STRING},
  telefono3: {type: Sequelize.STRING},
  telefono4: {type: Sequelize.STRING},
  telefono5: {type: Sequelize.STRING},
  telefono6: {type: Sequelize.STRING},
  telefono7: {type: Sequelize.STRING},
  observacion: {type: Sequelize.STRING}
},{
  validate: {
    isUnique: sequelize.validateIsUnique(['documento'],
    "'No se pudo realizar la accion! "+
    "Ya existe un cliente con el número de documento '+self.documento")
  }
});
this.Cliente.belongsTo(this.Ciudad,{onDelete:"restrict"});
this.Cliente.belongsTo(this.Estadocivil,{onDelete:"restrict"});

// Proveedor
exports.Proveedor = sequelize.define('proveedor', {
  razon_social: {type: Sequelize.STRING},
  documento: {type: Sequelize.STRING},
  direccion: {type: Sequelize.STRING},
  email: {type: Sequelize.STRING},
  web: {type: Sequelize.STRING},
  telefono1: {type: Sequelize.STRING},
  telefono2: {type: Sequelize.STRING},
  observacion: {type: Sequelize.STRING}
},{
  validate: {
    isUnique: sequelize.validateIsUnique(['documento'],
    "'No se pudo realizar la accion! "+"Ya existe un proveedor con el número de documento '+self.documento")
  }
});
this.Proveedor.belongsTo(this.Ciudad,{onDelete:"restrict"});

//Define IVA
exports.Iva = sequelize.define('iva', {
  descripcion: {type: Sequelize.STRING,allowNull: false, comment: 'Descripción'},
  porcentaje: {type: Sequelize.STRING,allowNull: false, comment: 'Porcentaje'}
},{
  validate: {
    isUnique: sequelize.validateIsUnique(['descripcion'],
    "'No se pudo realizar la accion! Ya existe el registro '+self.descripcion")
  }
});

// Deposito
exports.Deposito = sequelize.define('deposito', {
  descripcion: {type: Sequelize.STRING,allowNull: false, comment: 'Descripcion'},
  ubicacion: {type: Sequelize.STRING,allowNull: false, comment: 'Ubicacion'},
  estado: {type: Sequelize.BOOLEAN, comment: "1=true=activo, 0=false=inactivo"}
},{
  validate: {
    isUnique: sequelize.validateIsUnique(['descripcion'],
    "'No se pudo realizar la accion! "+"Ya existe un deposito con la descripcion '+self.descripcion")
  }
});

// Seccion
exports.Seccion = sequelize.define('seccion', {
  descripcion: {type: Sequelize.STRING,allowNull: false, comment: 'Descripción'}
},{
  validate: {
    isUnique: sequelize.validateIsUnique(['descripcion'],
    "'No se pudo realizar la accion! Ya existe la seccion '+self.descripcion")
  }
});

// Categoria
exports.Categoria = sequelize.define('categorias', {
  descripcion: {type: Sequelize.STRING,allowNull: false, comment: 'Descripción'}
},{
  validate: {
    isUnique: sequelize.validateIsUnique(['descripcion'],
    "'No se pudo realizar la accion! Ya existe la categoria '+self.descripcion")
  }
});

// Subcategoria
exports.Subcategoria = sequelize.define('subcategorias', {
  descripcion: {type: Sequelize.STRING,allowNull: false, comment: 'Descripción'}
},{
  validate: {
    isUnique: sequelize.validateIsUnique(['descripcion'],
    "'No se pudo realizar la accion! "+
    "Ya existe la subcategoria '+self.descripcion+"+"'para la categoria seleccionada'")
  }
});
this.Subcategoria.belongsTo(this.Categoria,{onDelete:"restrict"});

// Marca
exports.Marca = sequelize.define('marca', {
  descripcion: {type: Sequelize.STRING,allowNull: false, comment: 'Descripción'}
},{
  validate: {
    isUnique: sequelize.validateIsUnique(['descripcion'],
    "'No se pudo realizar la accion! Ya existe la marca '+self.descripcion")
  }
});

// Procedencia
exports.Procedencia = sequelize.define('procedencia', {
  descripcion: {type: Sequelize.STRING,allowNull: false, comment: 'Descripción'}
},{
  validate: {
    isUnique: sequelize.validateIsUnique(['descripcion'],
    "'No se pudo realizar la accion! Ya existe la procedencia '+self.descripcion")
  }
});

// Unidad de Medida
exports.Medida = sequelize.define('medida', {
  descripcion: {type: Sequelize.STRING,allowNull: false, comment: 'Descripcion'},
  unid_medida: {type: Sequelize.STRING,allowNull: false, comment: 'Unidad de medida'},
  acepta_decimal: {type: Sequelize.BOOLEAN, comment: "1=true=si acepta, 0=false=no acepta"}
  },{
  validate: {
    isUnique: sequelize.validateIsUnique(['descripcion'],
    "'No se pudo realizar la accion! "+
    "Ya existe una medida con la descripcion '+self.descripcion")
  }
});

// Producto
exports.Producto = sequelize.define('producto', {
  descripcion: {type: Sequelize.STRING,allowNull: false, comment: 'Descripción'},
  caracteristica: {type: Sequelize.STRING,allowNull: false, comment: 'Caracteristicas'},
  precomprapro: {type: Sequelize.STRING,allowNull: false, comment: 'precomprapro'},
  preunitario: {type: Sequelize.STRING,allowNull: false, comment: 'precomprabase'},
  preunitariovta: {type: Sequelize.STRING,allowNull: false, comment: 'precio1'},
  prepaquete: {type: Sequelize.STRING,allowNull: false, comment: 'precio2'},
  prepaquetevta: {type: Sequelize.STRING,allowNull: false, comment: 'precio3'},
  descuento: {type: Sequelize.BOOLEAN, comment: "1=true=si acepta, 0=false=no acepta"},
  cantpaq: {type: Sequelize.STRING,allowNull: false, comment: 'cantidad paquete'},
  sminunid: {type: Sequelize.STRING,allowNull: false, comment: 'stock minimo unitario'},
  sminpaq: {type: Sequelize.STRING,allowNull: false, comment: 'stock minimo paquete'},
  sreal: {type: Sequelize.STRING,allowNull: false, comment: 'stock real'}
},{
  validate: {
    isUnique: sequelize.validateIsUnique(['descripcion'],
    "'No se pudo realizar la accion! "+
    "Ya existe el producto '+self.descripcion+")
  }
});
this.Producto.belongsTo(this.Marca,{onDelete:"restrict"});
this.Producto.belongsTo(this.Categoria,{onDelete:"restrict"});
this.Producto.belongsTo(this.Subcategoria,{onDelete:"restrict"});
this.Producto.belongsTo(this.Medida,{onDelete:"restrict"});

// Materia prima
exports.Mat_prima = sequelize.define('materias', {
  descripcion: {type: Sequelize.STRING,allowNull: false, comment: 'Descripción'},
  precomprapro: {type: Sequelize.STRING,allowNull: false, comment: 'Precio compra promedio'},
  sminimo: {type: Sequelize.STRING,allowNull: false, comment: 'Stock minimo'},
  sreal: {type: Sequelize.STRING,allowNull: false, comment: 'Stock real'},
},{
  validate: {
    isUnique: sequelize.validateIsUnique(['descripcion'],
    "'No se pudo realizar la accion! Ya existe la materia prima '+self.descripcion")
  }
});
this.Mat_prima.belongsTo(this.Medida,{onDelete:"restrict"});

// Producto elaborado
exports.Elaborado = sequelize.define('elaborado', {
  descripcion: {type: Sequelize.STRING,allowNull: false, comment: 'Descripción'},
  caracteristica: {type: Sequelize.STRING,allowNull: false, comment: 'Caracteristicas'},
  costomanual: {type: Sequelize.STRING,allowNull: false, comment: 'CCosto Manual'},
  costoauto: {type: Sequelize.STRING,allowNull: false, comment: 'Costo Automatico'},
},{
  validate: {
    isUnique: sequelize.validateIsUnique(['descripcion'],
    "'No se pudo realizar la accion! "+ "Ya existe el producto '+self.descripcion+")
  }
});

exports.ElaboradoItem = sequelize.define("elaborado_items", {
   cantidad: {type: Sequelize.DOUBLE,allowNull: false},
   costo: {type: Sequelize.STRING,allowNull: false, comment: 'Costo'},
});
this.ElaboradoItem.belongsTo(this.Mat_prima,{onDelete:"restrict"});
this.Elaborado.hasMany(this.ElaboradoItem, {as: 'items',onDelete: 'CASCADE'});

//Define comprobantes
exports.Comprobante = sequelize.define('comprobante', {
  descripcion: {type: Sequelize.STRING,allowNull: false, comment: 'Descripción'},
  sigla: {type: Sequelize.STRING,allowNull: false, comment: 'Sigla'},
},{
  validate: {
    isUnique: sequelize.validateIsUnique(['descripcion'],
    "'No se pudo realizar la accion! Ya existe el comprobante '+self.descripcion")
  }
});

exports.Compra = sequelize.define('compra', {
  fechafactura: {type:Sequelize.DATEONLY,allowNull: false,comment: 'Fecha de la factura'},
  numcomprobante: {type:Sequelize.STRING,allowNull: false,comment: 'Numero del comprobante o factura'},
  numtimbrado: {type: Sequelize.STRING,comment: 'Número del timbrado'},
  venctimbrado: {type: Sequelize.DATEONLY,comment: 'Vencimiento del timbrado'},
  documento: {type: Sequelize.STRING, allowNull: false,comment: 'Número de documento o ruc'},
  razon_social: {type: Sequelize.STRING,allowNull: false,comment: 'Razon Social'},
  telefono1: {type: Sequelize.STRING,comment: 'Teléfono'},
  direccion: {type: Sequelize.STRING,comment: 'Dirección'},
  descuento_ex: {type: Sequelize.DOUBLE,comment: 'Descuento Exentas',defaultValue:0},
  descuento5: {type: Sequelize.DOUBLE,comment: 'Descuento IVA 5 %',defaultValue:0},
  descuento10: {type: Sequelize.DOUBLE,comment: 'Descuento IVA 10 %',defaultValue:0},
  subtot_ex: {type: Sequelize.DOUBLE,comment: 'Sub Total Exenta',defaultValue:0},
  subtot_5: {type: Sequelize.DOUBLE,comment: 'Sub Total 5 %',defaultValue:0},
  subtot_10: {type: Sequelize.DOUBLE,comment: 'Sub Total 10 %',defaultValue:0},
  total_comp: {type: Sequelize.DOUBLE,allowNull: false,comment: 'Total compra, sirve de verificacion'},
  total: {type: Sequelize.DOUBLE,allowNull: false,comment: 'Total',defaultValue:0},
  iva_5: {type: Sequelize.DOUBLE,comment: 'IVA 5 %',defaultValue:0},
  iva_10: {type: Sequelize.DOUBLE,comment: 'IVA 10 %',defaultValue:0},
  total_iva: {type: Sequelize.DOUBLE,comment: 'Total IVA',defaultValue:0},
  estado: {type: Sequelize.STRING(4), allowNull: false, comment: 'PEND=PENDIENTE, CONF=CONFIRMADO, ANUL=ANULADO', defaultValue:'PEND'}
});
this.Compra.belongsTo(this.Proveedor,{onDelete:"restrict"});
this.Compra.belongsTo(this.Comprobante,{onDelete:"restrict"});

exports.CompraItem = sequelize.define('compra_items', {
   cantidad: {type: Sequelize.STRING,allowNull: false,comment: 'Cantidad'},
   precunit: {type: Sequelize.DOUBLE,allowNull: false,comment: 'Precio unitario'},
   exenta: {type: Sequelize.DOUBLE,comment: 'Exenta',defaultValue:0},
   iva_cinco: {type: Sequelize.DOUBLE,comment: 'IVA Cinco',defaultValue:0},
   iva_diez: {type: Sequelize.DOUBLE,comment: 'IVA Diez',defaultValue:0}
});
this.CompraItem.belongsTo(this.Producto, {onDelete: "restrict"});
this.CompraItem.belongsTo(this.Mat_prima, {onDelete: "restrict"});
this.CompraItem.belongsTo(this.Iva,{onDelete:"restrict"});
this.Compra.hasMany(this.CompraItem, {as: 'items',onDelete: 'CASCADE'});


// OBLIGATORIO AL FINAL
sequelize.sync();
